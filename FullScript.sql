-- CREACION DE LA BASE DE DATOS
CREATE DATABASE IF NOT EXISTS AHEC;

-- CREACION DE LAS TABLAS PRINCIPALES

CREATE TABLE `AHEC`.`Evento` (
  `idEvento` MEDIUMINT NOT NULL AUTO_INCREMENT,
  `Titulo` VARCHAR(60) NOT NULL,
  PRIMARY KEY (`idEvento`));
  
  
CREATE TABLE `Grp_Objetos` (
  `idGrp_Objetos` mediumint(9) NOT NULL AUTO_INCREMENT,
  `Num_Inventario` mediumint(9) NOT NULL,
  `Alto` float NOT NULL,
  `Ancho` float NOT NULL,
  `Num_Volumenes` tinyint(4) NOT NULL DEFAULT '0',
  `Permite_Reproduccion` bit(1) NOT NULL,
  `Permite_Consulta` bit(1) NOT NULL,
  `Fecha_Catalogacion` date NOT NULL,
  `Fecha_Captura` date NOT NULL,
  `Observaciones` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`idGrp_Objetos`)
) ;




-- CATALOGOS

CREATE TABLE `Objeto_Individual` (
  `idObjeto_Individual` int(11) NOT NULL AUTO_INCREMENT,
  `Permitir_Consulta` bit(1) NOT NULL,
  `Ubicacion` tinyint(4) NOT NULL,
  `Estado_de_Conservacion` tinyint(4) NOT NULL,
  `Observaciones` varchar(500) NOT NULL,
  `Fecha_Donacion` varchar(45) NOT NULL,
  PRIMARY KEY (`idObjeto_Individual`),
  KEY `Ubicacion_idx` (`Ubicacion`),
  CONSTRAINT `FK_Ubicacion_Obj_Individual` FOREIGN KEY (`Ubicacion`) REFERENCES `Ubicaciones` (`idUbicaciones`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ;

-- PRINCIPAL
CREATE TABLE `AHEC`.`Ubicaciones` (
  `idUbicaciones` TINYINT NOT NULL AUTO_INCREMENT,
  `Ubicacion` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idUbicaciones`));
  
  -- CONTINUAR CATALOGOS
CREATE TABLE `Tipologia` (
  `idTipologia` tinyint(4) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(20) NOT NULL,
  `Tipo` varchar(45) NOT NULL,
  PRIMARY KEY (`idTipologia`),
  UNIQUE KEY `Tipo_UNIQUE` (`Tipo`)
) ;

CREATE TABLE `AHEC`.`Fondo` (
  `idFondo` TINYINT NOT NULL AUTO_INCREMENT,
  `Nombre` VARCHAR(25) NOT NULL,
  PRIMARY KEY (`idFondo`));

CREATE TABLE `AHEC`.`Coleccion` (
  `idColeccion` TINYINT NOT NULL AUTO_INCREMENT,
  `Nombre` VARCHAR(25) NOT NULL,
  PRIMARY KEY (`idColeccion`));


CREATE TABLE `AHEC`.`Genero` (
  `idGenero` TINYINT NOT NULL AUTO_INCREMENT,
  `Nombre` VARCHAR(25) NOT NULL,
  PRIMARY KEY (`idGenero`));


CREATE TABLE `AHEC`.`Pais` (
  `idPais` TINYINT NOT NULL AUTO_INCREMENT,
  `Nombre` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`idPais`));

CREATE TABLE `AHEC`.`Estante` (
  `idEstante` TINYINT NOT NULL AUTO_INCREMENT,
  `Numero` TINYINT NOT NULL,
  PRIMARY KEY (`idEstante`));

CREATE TABLE `AHEC`.`Caja` (
  `idCaja` INT NOT NULL AUTO_INCREMENT,
  `Numero` TINYINT NOT NULL,
  PRIMARY KEY (`idCaja`));

CREATE TABLE `AHEC`.`Soporte` (
  `idSoporte` TINYINT NOT NULL AUTO_INCREMENT,
  `Nombre` VARCHAR(35) NOT NULL,
  PRIMARY KEY (`idSoporte`));

CREATE TABLE `AHEC`.`Tecnida_Impresion` (
  `idTecnida_Impresion` TINYINT NOT NULL AUTO_INCREMENT,
  `Nombre` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`idTecnida_Impresion`));

CREATE TABLE `AHEC`.`Producción` (
  `idProducción` INT NOT NULL AUTO_INCREMENT,
  `Nombre` VARCHAR(40) NOT NULL,
  PRIMARY KEY (`idProducción`));
  
CREATE TABLE `AHEC`.`Estelar` (
  `idEstelar` MEDIUMINT NOT NULL AUTO_INCREMENT,
  `Nombre` VARCHAR(40) NOT NULL,
  PRIMARY KEY (`idEstelar`));


CREATE TABLE `AHEC`.`Director` (
  `idDirector` INT NOT NULL AUTO_INCREMENT,
  `Nombre` VARCHAR(40) NOT NULL,
  PRIMARY KEY (`idDirector`));

CREATE TABLE `AHEC`.`Archivo` (
  `idArchivo` TINYINT NOT NULL AUTO_INCREMENT,
  `Nombre` VARCHAR(55) NOT NULL,
  PRIMARY KEY (`idArchivo`));

CREATE TABLE `AHEC`.`Motivos_No_Consulta` (
  `idMotivos_No_Consulta` TINYINT NOT NULL AUTO_INCREMENT,
  `Nombre` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`idMotivos_No_Consulta`));

CREATE TABLE `AHEC`.`Capturista` (
  `idCapturista` TINYINT NOT NULL AUTO_INCREMENT,
  `Nombre` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idCapturista`));

CREATE TABLE `AHEC`.`Intervenciones` (
  `idIntervenciones` TINYINT NOT NULL AUTO_INCREMENT,
  `Nombre` VARCHAR(65) NOT NULL,
  PRIMARY KEY (`idIntervenciones`));

CREATE TABLE `AHEC`.`Estado_De_Deterioro` (
  `idEstado_De_Deterioro` TINYINT NOT NULL AUTO_INCREMENT,
  `Nombre` VARCHAR(65) NOT NULL,
  PRIMARY KEY (`idEstado_De_Deterioro`));

-- DETALLES


CREATE TABLE `Det_Estelares` (
  `idDet_Estelares` mediumint(9) NOT NULL AUTO_INCREMENT,
  `Clv_Estelar` mediumint(9) NOT NULL,
  `Clv_Evento` mediumint(9) NOT NULL,
  PRIMARY KEY (`idDet_Estelares`),
  KEY `FK_Estelares_idx` (`Clv_Estelar`),
  KEY `FK_Evento_idx` (`Clv_Evento`),
  CONSTRAINT `FK_Estelares` FOREIGN KEY (`Clv_Estelar`) REFERENCES `Estelar` (`idEstelar`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_Evento` FOREIGN KEY (`Clv_Evento`) REFERENCES `Evento` (`idEvento`) ON DELETE NO ACTION ON UPDATE NO ACTION
);

CREATE TABLE `AHEC`.`Det_Generos` (
  `idDet_Generos` MEDIUMINT NOT NULL AUTO_INCREMENT,
  `Clv_Genero` TINYINT NOT NULL,
  `Clv_Grp_Objeto` MEDIUMINT NOT NULL,
  PRIMARY KEY (`idDet_Generos`),
  INDEX `FK_Generos_idx` (`Clv_Genero` ASC),
  INDEX `FK_Generos_Grp_Objeto_idx` (`Clv_Grp_Objeto` ASC),
  CONSTRAINT `FK_Generos_Genero`
    FOREIGN KEY (`Clv_Genero`)
    REFERENCES `AHEC`.`Genero` (`idGenero`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_Generos_Grp_Objeto`
    FOREIGN KEY (`Clv_Grp_Objeto`)
    REFERENCES `AHEC`.`Grp_Objetos` (`idGrp_Objetos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

CREATE TABLE `AHEC`.`Det_Paises` (
  `idDet_Paises` MEDIUMINT NOT NULL AUTO_INCREMENT,
  `Clv_Pais` TINYINT NOT NULL,
  `Clv_Grp_Objeto` MEDIUMINT NOT NULL,
  PRIMARY KEY (`idDet_Paises`),
  INDEX `FK_Paises_Grp_Objetos_idx` (`Clv_Grp_Objeto` ASC),
  INDEX `FK_Paises_Pais_idx` (`Clv_Pais` ASC),
  CONSTRAINT `FK_Paises_Pais`
    FOREIGN KEY (`Clv_Pais`)
    REFERENCES `AHEC`.`Pais` (`idPais`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_Paises_Grp_Objetos`
    FOREIGN KEY (`Clv_Grp_Objeto`)
    REFERENCES `AHEC`.`Grp_Objetos` (`idGrp_Objetos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


CREATE TABLE `Det_Motivos_No_Consulta` (
  `idDet_Motivos_No_Consulta` int(11) NOT NULL AUTO_INCREMENT,
  `Clv_Motivos` tinyint(4) NOT NULL,
  `Clv_Objeto_Individual` int(11) NOT NULL,
  PRIMARY KEY (`idDet_Motivos_No_Consulta`),
  KEY `FK_Motivos_No_Consulta_Motivos_idx` (`Clv_Motivos`),
  KEY `FK_Motivos_No_Consulta_Objeto_Individual_idx` (`Clv_Objeto_Individual`),
  CONSTRAINT `FK_Motivos_No_Consulta_Motivos` FOREIGN KEY (`Clv_Motivos`) REFERENCES `Motivos_No_Consulta` (`idMotivos_No_Consulta`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_Motivos_No_Consulta_Objeto_Individual` FOREIGN KEY (`Clv_Objeto_Individual`) REFERENCES `Objeto_Individual` (`idObjeto_Individual`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ;


CREATE TABLE `Det_Intervenciones` (
  `idDet_Intervenciones` int(11) NOT NULL AUTO_INCREMENT,
  `Clv_Intervenciones` tinyint(4) NOT NULL,
  `Clv_Objeto_Individual` int(11) NOT NULL,
  `Fecha_Realización` date DEFAULT NULL,
  PRIMARY KEY (`idDet_Intervenciones`),
  KEY `FK_Intervenciones_Intervencion_idx` (`Clv_Intervenciones`),
  KEY `FK_Intervenciones_Objeto_Individual_idx` (`Clv_Objeto_Individual`),
  CONSTRAINT `FK_Intervenciones_Intervencion` FOREIGN KEY (`Clv_Intervenciones`) REFERENCES `Intervenciones` (`idIntervenciones`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_Intervenciones_Objeto_Individual` FOREIGN KEY (`Clv_Objeto_Individual`) REFERENCES `Objeto_Individual` (`idObjeto_Individual`) ON DELETE NO ACTION ON UPDATE NO ACTION
);
    

CREATE TABLE `AHEC`.`Det_Estados_De_Deterioro` (
  `idDet_Esatdos_De_Conservacion` INT NOT NULL AUTO_INCREMENT,
  `Clv_Estado_De_Conservacion` TINYINT NOT NULL,
  `Clv_Objeto_Individual` INT NOT NULL,
  PRIMARY KEY (`idDet_Esatdos_De_Conservacion`),
  INDEX `FK_Estados_De_Deterioro_Objeto_Individual_idx` (`Clv_Objeto_Individual` ASC),
  INDEX `FK_Estados_de_Deterioro_Estados_idx` (`Clv_Estado_De_Conservacion` ASC),
  CONSTRAINT `FK_Estados_de_Deterioro_Estados`
    FOREIGN KEY (`Clv_Estado_De_Conservacion`)
    REFERENCES `AHEC`.`Estado_De_Deterioro` (`idEstado_De_Deterioro`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_Estados_De_Deterioro_Objeto_Individual`
    FOREIGN KEY (`Clv_Objeto_Individual`)
    REFERENCES `AHEC`.`Objeto_Individual` (`idObjeto_Individual`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

CREATE TABLE `AHEC`.`Det_Capturistas` (
  `idDet_Capturistas` INT NOT NULL AUTO_INCREMENT,
  `Clv_Capturista` TINYINT NOT NULL,
  `Clv_Objeto_Individual` INT NOT NULL,
  `Fecha_Modificacion` DATE NOT NULL,
  PRIMARY KEY (`idDet_Capturistas`),
  INDEX `FK_Capturistas_Capturista_idx` (`Clv_Capturista` ASC),
  INDEX `FK_Capturistas_Objeto_Individual_idx` (`Clv_Objeto_Individual` ASC),
  CONSTRAINT `FK_Capturistas_Capturista`
    FOREIGN KEY (`Clv_Capturista`)
    REFERENCES `AHEC`.`Capturista` (`idCapturista`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_Capturistas_Objeto_Individual`
    FOREIGN KEY (`Clv_Objeto_Individual`)
    REFERENCES `AHEC`.`Objeto_Individual` (`idObjeto_Individual`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);





















