<?php
  include 'php/LoadData.php';
?>

<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="images/Dibujo.ico">
    <title>AHEC</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/styleof.css">
    <link rel="stylesheet" href="css/animate.min.css">
    <script src="js/jquery.js"></script>

    <link href="https://fonts.googleapis.com/css?family=Roboto|Roboto+Slab" rel="stylesheet">
    <script src="js/back.js"></script>
    <script src="js/menu.js"></script>
  </head>

  <body class="text-dark" id="body">

    <div id="sidebar" class="sidebar collapse animated">
      <h4 class="animated slideInLeft">Menú<span id="cerrarMenu"><i class="fas fa-chevron-left"></i></span></h4>
      <ul>

        <li><a id="consulta" class="animated slideInRight" href="#" data-toggle="modal" data-target="#modalConsulta"><i class="fas fa-search"></i> Consultar</a></li>
        <li><a id="nuevo"   class="animated slideInRight" href="#" data-toggle="modal" data-target="#modalAgredarCartel"><i class="far fa-plus-square"></i> Agregar</a></li>
        <li><a id="salir" class="deloff animated slideInRight" href="#"><i class="fas fa-sign-out-alt"></i> Cerrar Sesión</a></li>
      </ul>
      <img  src="images/back.png" width="200" class="navb d-flex align-items-end animated slideInDown" alt="">
    </div>
    <div class="contenido">
      <span id="abrirMenu"><i class="fas fa-list-ul"></i></span>
    </div>


    <div class="plus-element form-group row">
      <div class="col-sm-4 row">
        <label class="">Agregar elemento</label>
        <button type="button" class="btn btn-blur form-group ml-2" data-toggle="modal" data-target="#modalAgregarElemento">+</button>
      </div>

      </div>

  <!-- Inico de barra de Menú-->
        <ul>
          <ul class="tabs col-sm-10">
            <li><a href="#tab1" id="pestaña1"><span class="fas fa-archive second-color"></span> <span class="tab-text">General</span></a></li>
            <li><a href="#tab2" id="pestaña2"><span class="fas fa-film second-color"></span> <span class="tab-text">Descripción</span></a></li>
            <li><a href="#tab3" id="pestaña3"><span class="fas fa-portrait second-color"></span> <span class="tab-text">Características</span></a></li>
            <li><a href="#tab4" id="pestaña4"><span class="fas fa-align-center second-color"></span> <span class="tab-text">Accesibilidad</span></a></li>
          </ul>
        </ul>
  <!--Fin de barra de Menú -->

  <!--<div><? //echo error($usuario); ?></div>-->
  <!-- Formulario-->
  <form id="form" name="form" enctype="multipart/form-data" method="post" action="php/agregarCartel.php">



    <div class="container col-sm-10 d-flex justify-content-center">
      <section class="container secciones">
          <article id="tab1">
            <div class="col-sm-12 row form-group d-flex justify-content-between">
              <div class="col-sm-6 row d-flex justify-content-center align-items-center">



                <label for="numero_inventario" class="col-sm-6 form-control-label">Número de inventario:</label>
                <input id="NumInventario" type="number" class="col-sm-4 form-control" name="numero_inventario" min="0" max="99999" placeholder="#" required="required">
              </div>
              <div class="col-sm-6 row d-flex justify-content-center align-items-center">
                <label for="titulo" class="col-sm-2 form-control-label">Título:</label>
                <input class="form-control col-sm-10" id="titulo" placeholder="Título del filme" type="text" name="titulo" required="required">
              </div>
            </div>

          <br>

          <div class="col-sm-12 row d-flex justify-content-between">
            <div class="form-group row col-sm-4 d-flex align-items-center">
              <label for="tipología" class="col-sm-4 form-control-label">Tipologia:</label>
                <select class="select-class form-control col-sm-8 point" id="tipologia" name="tipologia">
                  <script>
                      var nombreTipologias=<?php echo json_encode($nombreTipologias);?>;
                      var idTipologias=<?php echo json_encode($idTipologias);?>;

                      for(var row = 0; row < nombreTipologias.length; row ++ ){
                        document.write("<option value=\""+idTipologias[row] +"\" >"+nombreTipologias[row]+" </option>");
                      }
                  </script>
                </select>
            </div>
            <div class="form-group row col-sm-4 d-flex align-items-center">
              <label for="fondo" class="col-sm-3 form-control-label">Fondo:</label>
                <select class="select-class form-control col-sm-9 point" id="fondo" name="fondo">
                  <script>

                      var nombreFondos=<?php echo json_encode($nombreFondo);?>;
                      var idFondos=<?php echo json_encode($idFondo);?>;

                      for(var row = 0; row < nombreFondos.length; row ++ ){
                        document.write("<option value=\""+idFondos[row] +"\" >"+nombreFondos[row]+" </option>");
                      }
                  </script>
                </select>

            </div>

            <div class="form-group col-sm-4 row d-flex align-items-center">
              <label for="coleccion" class="col-sm-4 form-control-label">Colección:</label>
                <select class="select-class form-control col-sm-8 point" id="coleccion" name="coleccion">
                  <script>

                      var nombreColecciones=<?php echo json_encode($nombreColeccion);?>;
                      var idColecciones=<?php echo json_encode($idColeccion);?>;
                      for(var row = 0; row < nombreColecciones.length; row ++ ){
                        document.write("<option value=\""+idColecciones[row] +"\" >"+nombreColecciones[row]+" </option>");
                      }
                  </script>
                </select>
            </div>
          </div>
          <br>

              <div class="form-group row col-sm-12 d-flex justify-content-between" id="contenedor_topografico">

                <div class="form-group row col-sm-3 d-flex justify-content-center d-flex align-items-center">
                  <label for="caja" class="col-sm-4 form-control-label">Caja:</label>
                  <select class="select-class form-control col-sm-5 point" id="caja" name="caja">
                    <script>
                        var cajas=<?php echo json_encode($idCajas);?>;
                        var numeros=<?php echo json_encode($nombreCajas);?>;
                        for(var row = 0; row < cajas.length; row ++ ){
                          document.write("<option value=\""+numeros[row] +"\" >"+cajas[row]+" </option>");
                        }
                    </script>
                  </select>

                </div>

                <div class="form-group row col-sm-3 d-flex justify-content-center d-flex align-items-center">
                  <label for="estante" class="col-sm-5 form-control-label">Estante:</label>
                  <select class="select-class form-control col-sm-5 point" id="estante" name="estante">
                    <script>

                        var estantes=<?php echo json_encode($idEstantes);?>;
                        var numeroEstantes=<?php echo json_encode($nombreEstantes);?>;

                        for(var row = 0; row < estantes.length; row ++ ){
                          document.write("<option value=\""+numeroEstantes[row] +"\" >"+estantes[row]+" </option>");
                        }

                    </script>
                  </select>
                  <!--<button type="button" class="btn btn-blur" data-toggle="modal" data-target="#modalEstante">+</button>-->
                </div>

                <div class="form-group row col-sm-3 d-flex align-items-center">
                  <label for="volumenes" class="col-sm-6 form-control-label">Volumenes:</label>
                  <input id="volumenes" type="number" name="volumenes" value="1" class="col-sm-5 form-control ml-2">
                </div>
              </div>

              <h5 class="text-center">Seleccione las imágenes correspondientes</h5>

                <div class="row d-flex justify-content-center">
                  <div class="contImg" id="first"></div>
                  <div class="divFile">
                    <p class="texto"> <i class="fas fa-plus"></i> </p>
                    <input id="files" type="file" name="uploadimg[]" class="entrada" multiple>
                  </div>
                </div>



            </article>

      <article id="tab2">
        <div class="container">

            <div class="col-sm-12 row d-flex justify-content-start">
              <div class="form-group col-sm-7 row d-flex justify-content-center d-flex align-items-center">
                <label class="form-control-label col-sm-4" for="fecha_donacion">Fecha de estreno:</label>
                <input id="estreno" class="col-sm-4 form-control ml-2" type="date" name="fecha_estreno" required="required">
              </div>
            </div>

            <div class="col-sm-12 row d-flex justify-content-center">
              <div class="col-sm-2 row d-flex justify-content-end align-items-center">
                <label class="form-control-label col-sm-9" for="director">Director:</label>
              </div>
              <div class="form-group row col-sm-8 ml-2">
                <div id="director" class="form-control"></div>
                <script>
                    var nombreDirectores=<?php echo json_encode($nombreDirector);?>;
                    var idDirectores=<?php echo json_encode($idDirector);?>;
                </script>
              </div>
            </div>

            <div class="col-sm-12 row d-flex justify-content-center">
              <div class="col-sm-2 row d-flex justify-content-end align-items-center">
                <label class="form-control-label col-sm-9"for="produccion">Producción:</label>
              </div>
              <div class="form-group row col-sm-8 ml-2">
                <div id="produccion" class="form-control"></div>
                <script>
                  var nombreProductores=<?php echo json_encode($nombreProductores);?>;
                  var idProductores=<?php echo json_encode($idProductores);?>;
                </script>
              </div>
            </div>


            <div class="col-sm-12 row d-flex justify-content-center">
              <div class="col-sm-2 row d-flex justify-content-end align-items-center">
                <label for="estelares" class="form-control-label col-sm-9">Estelares:</label>
              </div>
              <div class="form-group row col-sm-8 ml-2">
                <div id="estelares" class="form-control"></div>
                <script>
                  var nombreEstelares=<?php echo json_encode($nombreEstelares);?>;
                  var idEstelares=<?php echo json_encode($idEstelares);?>;
                </script>
              </div>
            </div>




            <div class="col-sm-12 row d-flex justify-content-center">
              <div class="col-sm-2 row d-flex justify-content-end align-items-center">
                <label class="form-control-label col-sm-9" for="genero">Genero:</label>
              </div>
              <div class="form-group row col-sm-8 ml-2">
                <div id="genero" class="form-control" name="genero"></div>
                <script>
                  var nombreGeneros=<?php echo json_encode($nombreGeneros);?>;
                  var idGeneros=<?php echo json_encode($idGeneros);?>;


                </script>
              </div>


            </div>



            <div class="col-sm-12 row d-flex justify-content-center">
              <div class="col-sm-2 row d-flex justify-content-end align-items-center">
                <label for="pais" class="form-control-label col-sm-9">País:</label>
              </div>
              <div class="form-group row col-sm-8 ml-2">
                <div class="form-control" id="pais">    </div>
                <script>
                  var nombrePaises=<?php echo json_encode($nombrePaises);?>;
                  var idPaises=<?php echo json_encode($idPaises);?>;
                </script>
              </div>
            </div>
          </div>
      </article>



     <article id="tab3">

       <div class="from-group row col-sm-12 d-flex justify-content-center">

         <div class="form-group row col-sm-4 d-flex justify-content-center d-flex align-items-center">
           <label for="soporte" class="form-control-label col-sm-4 mb-2">Soporte:</label>
           <select class="select-class form-control col-sm-8 point mb-2" name="soporte" id="soporte" >
             <script>
                 var nombreSoportes=<?php echo json_encode($nombreSoportes);?>;
                 var idSoportes=<?php echo json_encode($idSoportes);?>;

                 for(var row = 0; row < nombreSoportes.length; row ++ ){
                   document.write("<option value=\""+idSoportes[row] +"\" >"+nombreSoportes[row]+" </option>");
                 }

             </script>
           </select>

         </div>

         <div class="form-group row col-sm-4 d-flex justify-content-center d-flex align-items-center">
           <label for="impresion" class="form-control-label col-sm-4 mb-2">Técnica:</label>
           <select class="select-class form-control col-sm-8 point mb-2" name="impresion" id="impresion">
             <script>
                 var nombreTecnicas=<?php echo json_encode($nombreTecnicas);?>;
                 var idTecnicas=<?php echo json_encode($idTecnicas);?>;

                 for(var row = 0; row < nombreTecnicas.length; row ++ ){
                   document.write("<option value=\""+idTecnicas[row] +"\" >"+nombreTecnicas[row]+" </option>");
                 }

             </script>
           </select>

         </div>



       </div>

       <div class="from-group row col-sm-12 d-flex justify-content-center">
         <div class="form-group row col-sm-4 d-flex justify-content-center">
           <div class="form-group row col-sm-12 d-flex align-items-center">
             <label for="alto" class="form-control-label col-sm-3">Alto:</label>
             <input id="alto" type="number" name="alto" value="0.0" step="0.1" min="0" class="form-control col-sm-4" ind="alto" required="required">
           </div>
         </div>

         <div class="form-group row col-sm-4 d-flex justify-content-center">
           <div class="form-group row col-sm-12 d-flex align-items-center">
             <label for="ancho" class="form-control-label col-sm-4">Ancho:</label>
             <input type="number" name="ancho" value="0.0" step="0.1" min="0" class="form-control col-sm-4" id="ancho" required="required">
           </div>
         </div>


         <div class="form-group col-sm-4 bord">
           <div class="d-flex justify-content-start">
             <label for="consulta" class="col-sm-6 mart">Tipo medidas:  </label>
           </div>
           <div class=" d-flex justify-content-center">
             <label class="radio-inline col-sm-2 point "><input type="radio" value="false" name="consulta" class="point" id="cm"> Cm</label>
             <label class="radio-inline col-sm-2 point "><input type="radio" value="true" name="consulta" class="point" id="pul">Pul</label>
           </div>
         </div>

       </div>

       <div class="col-sm-12 row d-flex justify-content-start align-items-center ml-4 mt-3">
         <div class="col-sm-3 row d-flex justify-content-center align-items-center">
           <label for="conservacion" class="form-control-label">Estado de conservación:</label>
         </div>
         <div class="form-group row col-sm-8">
           <div class="form-control" id="estado_conservacion">    </div>
           <script>
           var nombreDeterioros=<?php echo json_encode($nombreDeterioros);?>;
           var idDeterioros=<?php echo json_encode($idDeterioros);?>;
           </script>
         </div>
       </div>

       <div class="col-sm-12 row d-flex justify-content-center">
         <div class="col-sm-2 row d-flex justify-content-end align-items-center">
           <label for="intervencionS" class="form-control-label">Interv. sugerida:</label>
         </div>
         <div class="form-group row col-sm-8 ml-1">
           <div class="form-control" id="intervencionS">    </div>
           <script>
            var nombreIntervenciones=<?php echo json_encode($nombreIntervenciones);?>;
            var idIntervenciones=<?php echo json_encode($idIntervenciones);?>;
           </script>
         </div>
       </div>

       <div class="col-sm-12 row d-flex justify-content-center">
         <div class="col-sm-2 row d-flex justify-content-end align-items-center">
           <label for="intervencionR" class="form-control-label">Interv. realizada:</label>
         </div>
         <div class="form-group row col-sm-8 ml-1">
           <div class="form-control" id="intervencionR">    </div>
           <script>
            var nombreIntervenciones=<?php echo json_encode($nombreIntervenciones);?>;
            var idIntervenciones=<?php echo json_encode($idIntervenciones);?>;
           </script>
         </div>
       </div>

     </article>



        <article id="tab4">
<!--   cosas de segundo plano-->
  <div class="form-group col-sm-12 row d-flex justify-content-center">
    <div class="form-group col-sm-3 bord">
      <div class="d-flex justify-content-center">
        <label for="consulta" class="col-sm-10 mart">Permite consulta:  </label>
      </div>
      <div class=" d-flex justify-content-center">
        <label class="radio-inline  point col-sm-2"><input id="consultaV" type="radio" name="Consulta" value="true" class="point"> Sí</label>
        <label class="radio-inline  point"><input id="consultaF" type="radio" name="Consulta" value="false" class="point" data-toggle="modal" > No</label>
      </div>
    </div>
    <div class="form-group col-sm-3 bord">
      <div class="d-flex justify-content-center">
        <label for="reproduccion" class="col-sm-10 mart">Permite reproduccion:  </label>
      </div>
      <div class=" d-flex justify-content-center">
        <label class="radio-inline point col-sm-2"><input id="reproduccionV" type="radio" value="true" name="reproduccion" class="point"> Sí</label>
        <label class="radio-inline point"><input id="reproduccionF" type="radio" value="false" name="reproduccion" class="point"> No</label>
      </div>
    </div>
    <div class="form-group col-sm-3 bord">
      <div class="d-flex justify-content-center">
        <label for="digital" class="col-sm-10 mart">Reproduccion digital:  </label>
      </div>
      <div class=" d-flex justify-content-center">
        <label class="radio-inline point col-sm-2"><input id="digitalV" type="radio" value="true" name="digital" class="point"> Sí</label>
        <label class="radio-inline point"><input id="digitalF" type="radio" value="false" name="digital" class="point"> No</label>
      </div>
    </div>
  </div>


  <div class=" col-sm-12 row d-flex justify-content-start align-items-center ml-4">
    <div class=" col-sm-3 row d-flex justify-content-center align-items-center">
      <label id="lblNoConsulta" for="conservacion" class="collapse form-control-label">Motivo:</label>
    </div>
    <div id="selectNoConsulta" class="collapse form-group row col-sm-8">
      <div class="form-control" id="motivo_No_Consulta"></div>
      <script>
        var nombreMotivosNoConsulta=<?php echo json_encode($nombreMotivosNoConsulta);?>;
        var idMotivosNoConsulta=<?php echo json_encode($idMotivosNoConsulta);?>;

      </script>
    </div>
  </div>


  <div class="col-sm-12 d-flex justify-content-center">
    <label for="observaciones">Observaciones:</label>
    <textarea class="form-control col-sm-8 ml-1" name="observaciones" placeholder="Escriba las observaciones del fondo" id="observaciones"></textarea>
  </div>

  <br>


  <div class="form-group row col-sm-12 d-flex justify-content-center">
    <div class="col-sm-6 row d-flex justify-content-center">
      <div class="col-sm-4 row d-flex justify-content-end align-items-center">
        <label for="catalogador">Catalogador:</label>
      </div>
      <select class="form-control col-sm-8 ml-1 point" id="catalogador" name="catalogador">
        <script>
        var nombreCatalogadores=<?php echo json_encode($nombreCatalogadores);?>;
        var idCatalogadores=<?php echo json_encode($idCatalogadores);?>;

        for(var row = 0; row < nombreCatalogadores.length; row ++ ){
          document.write("<option value=\""+idCatalogadores[row] +"\" >"+nombreCatalogadores[row]+" </option>");
        }

      </script>
    </select>
  </div>

  <div class="col-sm-6 row d-flex justify-content-center">
    <div class="col-sm-4 row d-flex justify-content-end align-items-center">
      <label for="capturista" class="">Capturista:</label>
    </div>
    <select class="form-control col-sm-8 ml-1 point" id="capturista" name="capturista">
      <script>
      var nombreCapturistas=<?php echo json_encode($nombreCapturistas);?>;
      var idCapturistas=<?php echo json_encode($idCapturistas);?>;

      for(var row = 0; row < nombreCapturistas.length; row ++ ){
        document.write("<option value=\""+idCapturistas[row] +"\" >"+nombreCapturistas[row]+" </option>");
      }

    </script>
    </select>
  </div>

  </div>

  <div class="form-group row d-flex justify-content-end justify-content-center">
    <div class="form-group row col-sm-5 justify-content-center align-items-center">
      <label for="archivo" class="form-control-label col-sm-3 mt-3">Archivo: </label>
      <select class="select-class form-control col-sm-4 point mt-3" name="archivo" id="archivo">
        <script>
        var nombreArchivos=<?php echo json_encode($nombreArchivos);?>;
        var idArchivos=<?php echo json_encode($idArchivos);?>;
        for(var row = 0; row < nombreArchivos.length; row ++ ){
          document.write("<option value=\""+idArchivos[row] +"\" >"+nombreArchivos[row]+" </option>");
        }

      </script>
    </select>
  </div>
  <div class="col-sm-5 row d-flex justify-content-center align-items-center">
    <label for="fecha_catalogador" class="form-control-label col-sm-6">Fecha catalogación:</label>
    <input type="date" name="fecha_Catalogacion" class="form-control col-sm-6" id="fecha_Catalogacion">
  </div>
  </div>

  </article>
  </section>
  </div>
  <div class="col-sm-10 container menLeft d-flex justify-content-end ">
    <button id="registro" type="submit" class="btn btn-danger mb-3 col-sm-2 point mariel">Registrar</button>
  </div>
  </form>

         <!-- Fin de formulario-->

    <!-- MODAL GENERAL-->
    <div class="modal fade" id="modalAgregarElemento" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Agregar elemento</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form method="post" action="php/agregarElemento.php">
            <div class="modal-body">
              <div class="container form-group row">
                <label for="tipo_Elemento" class="col-sm-12 text-center">Seleccione el tipo de elemento:</label>
                <select class="select-class form-control" name="tipo_Elemento" id="tipo_Elemento" name="tipo_Elemento">
                  <option value="1">Archivo</option>
                  <option value="2">Capturista</option>
                  <option value="3">Catalogador</option>
                  <option value="4">Colección</option>
                  <option value="5">Fondo</option>
                  <option value="6">Soporte</option>
                  <option value="7">Técnica de impresión</option>
                  <option value="8">Tipología</option>
                </select>
                <br/>
                <br>
                <label class="col-sm-12 text-center" for="nombre_Elemento">Escriba el nombre del elemento:</label>
                <input class="form-control" id="nombre_Elemento" nombre="nombre_Elemento" placeholder="Nombre del elemento" type="text" name="nombre_Elemento">
              </div>
            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-success">Aceptar</button>
              <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!-- FIN DEL MODAL GENERAL -->

    <!-- MODAL DE CONSULTA-->
    <div class="modal fade" id="modalConsulta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Consulta de filme</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

            <div class="modal-body">
              <form method="post" action="consultaMultiple.php">
              <div class="container form-group row d-flex justify-content-center">
                <label class="col-sm-4 text-center mt-2" for="tipo_Consulta">Seleccionar tipo de consulta:</label>
                <select class="select-class form-control col-sm-6"  id="tipo_Consulta" name="tipo_Consulta">
                  <option value="1">Número de inventario</option>
                  <option value="2">Titulo</option>
                  <option value="3">Rangos de fechas</option>
                  <option value="4">Director</option>
                  <option value="5">Estelares</option>
                  <option value="6">Produccion</option>
                  <option value="7">Cualquier coincidencia</option>
                </select>

              </div>

              <div id="porInventario" class="row mt-2 collapse">
                <div class="row d-flex justify-content-center">
                  <label id="lblInventarioConsulta" for="inventarioConsulta" class="col-sm-3 form-control-label mt-1">Número de inventario:</label>
                  <input id="inventarioConsulta" type="number" class="col-sm-2 form-control" name="inventarioConsulta" min="0" max="99999" placeholder="#">
                </div>
              </div>
              <div id="porTitulo" class="row mt-2 collapse">
                <div class="row d-flex justify-content-center">
                  <label id="lblTituloConsulta" for="tituloConsulta" class="col-sm-1 form-control-label mt-1">Título:</label>
                  <input class="form-control col-sm-8 ml-2" id="tituloConsulta" placeholder="Título del filme" type="text" name="tituloConsulta">
                </div>
              </div>
              <div id="porFechas" class="mt-2 collapse">
                <div class="row justify-content-center">
                  <label id="lblFechaInicio" class="form-control-label col-sm-3 mt-1" for="fecha_donacion">Fecha de inicio:</label>
                  <input id="fechaInicio" class="col-sm-4 form-control mb-3" type="date" name="fechaInicial">
                </div>
                <div class="row justify-content-center">
                  <label id="lblFechaFinal" class="form-control-label col-sm-3 mt-1" for="fecha_donacion">Fecha final:</label>
                  <input id="fechaFinal" class="col-sm-4 form-control" type="date" name="fechaFinal">
                </div>
              </div>
              <div id="porDirector" class="row mt-2 collapse">
                <div class="row d-flex justify-content-center">
                  <label id="lblDirectorConsulta" for="directorConsulta" class="col-sm-1 form-control-label mt-1">Director:</label>
                  <input class="form-control col-sm-8 ml-3" id="directorConsulta" placeholder="Nombre del director" type="text" name="directorConsulta" >
                </div>
              </div>
              <div id="porEstelares" class="row mt-2 collapse">
                <div class="row d-flex justify-content-center">
                  <label id="lblEstelarConsulta" for="estelarConsulta" class="col-sm-1 form-control-label mt-2">Estelar:</label>
                  <input class="form-control col-sm-8 ml-2" id="estelarConsulta" placeholder="Nombre del estelar" type="text" name="estelarConsulta">
                </div>
              </div>
              <div id="porProduccion" class="row mt-2 collapse">
                <div class="row d-flex justify-content-center">
                  <label id="lblTituloConsulta" for="tituloConsulta" class="col-sm-1 form-control-label mt-1">Producción:</label>
                  <input class="form-control col-sm-8 ml-2" id="nombreProduccion" placeholder="Nombre de la producción" type="text" name="nombreProduccion">
                </div>
              </div>
              <div id="porCoincidencia" class="row mt-2 collapse">
                <div class="row d-flex justify-content-center">
                  <label id="lblTituloConsulta" for="tituloConsulta" class="col-sm-1 form-control-label mt-1">Nombre:</label>
                  <input class="form-control col-sm-8 ml-2" id="nombreCoincidencia" placeholder="Nombre, palabra o fragmento" type="text" name="nombreCoincidencia">
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button id="btnAceptarModal"type="submit" class="btn btn-success">Aceptar</button>
              <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
          </form>

        </div>
      </div>
    </div>
    <!-- FIN DEL MODAL DE CONSULTA -->


    <!-- MODAL DE AGREGADO-->
    <div class="modal fade" id="modalAgregarCartel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Agregar cartel</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

            <div class="modal-body">
              <form method="post" action="agregarElementoCartelExistente.php">
              <div class="container form-group row d-flex justify-content-center">
                <label class="col-sm-4 text-center mt-2" for="tipo_Cartel">Seleccionar tipo de registro:</label>
                <select class="select-class form-control col-sm-6" name="tipo_Cartel" id="tipo_Cartel">
                  <option value="1">Nuevo registro</option>
                  <option value="2">Agregar cartel a registro existente</option>
                </select>
              </div>

              <div id="agregarAExistente" class="row mt-2 collapse">
                <div class="row d-flex justify-content-center">
                  <label id="lblInventarioAgregadoCartel" for="inventarioConsulta" class="col-sm-3 form-control-label mt-1">Número de inventario:</label>
                  <input id="inventarioAgregadoCartel" type="number" class="col-sm-2 form-control" name="inventarioAgregadoCartel" min="0" max="99999" placeholder="#">
                </div>
              </div>



            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-success">Aceptar</button>
              <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>

            </div>
          </form>

        </div>
      </div>
    </div>


<!-- FIN DEL MODAL DE AGREGADO DE ELEMENTO -->

    <footer>
      <div class="row">
        <div class="col-sm-3 float-right">
            <img  src="images/cultura_colima.png" width="200" class="float-right" alt="">
        </div>
        <div class="col-sm-6">
          <br>
          <div class="text-center">Juárez y Díaz Mirón s/n Colima, Col. CP 28000. Colonia Centro</div>
          <div class="text-center">Tel. (312) 313 9993</div>
          <div class="text-center">info@archivohistoricocolima.mx</div>
          <div class="text-center">© 2018 Secretaría de Cultura</div>
          <div class="text-center">Gobierno del Estado de Colima</div>
        </div>
        <div class="col-sm-3 float-left">
            <img  src="images/secretaria_de_cultura.jpg" width="200" class="float-left" alt="">
        </div>
      </div>

    </footer>


    <!--Este "script" es para los iconos-->
    <script defer src="https://use.fontawesome.com/releases/v5.0.12/js/all.js" integrity="sha384-Voup2lBiiyZYkRto2XWqbzxHXwzcm4A5RfdfG6466bu5LqjwwrjXCMBQBLMWh7qR" crossorigin="anonymous"></script>
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/imagenes.js"></script>
    <link href="MagicSuggest/magicsuggest-min.css" rel="stylesheet">
    <script src="MagicSuggest/magicsuggest-min.js"></script>
    <script src="js/navbar.js"></script></script>
    <script src="js/magicsuggest.js"></script>
    <script src="js/multSelec.js"></script>
    <script src="js/images.js"></script>
    --<script src="js/noConsulta.js"></script>
    <script src="js/consulta.js"></script
    <script src="js/habilitarPestañas.js"></script
  </body>
</html>
