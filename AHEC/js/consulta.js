$(document).ready(function(){


  $('#nuevo').click(function(){
    $('#modalAgregarCartel').modal('show');
    $('#sidebar').slideUp(600);
    $('#abrirMenu').css({
        'visibility': 'visible'
    });
    var valor;

    $('#tipo_Cartel').change(function(){
      valor = $('#tipo_Cartel').val();
      agregarCartel(valor);
    });

  });

  $('#consulta').click(function(){
    $('#modalConsulta').modal('show');
    $('#sidebar').slideUp(600);
    $('#abrirMenu').css({
        'visibility': 'visible'
    });

    var valor;
    var chg = $('#tipo_Consulta').val();
    if(chg > 0){
      valor = $('#tipo_Consulta').select().val();
    }
    $('#tipo_Consulta').change(function(){
      valor = $('#tipo_Consulta').val();
      consulta(valor);
    });
    consulta(valor);
  });

});


function agregarCartel(valor){
  if(valor == 2){
    $('#agregarAExistente').show(600);
    $('#inventarioAgregadoCartel').attr('required','required');
  }else{
    $('#agregarAExistente').slideUp(600);
    $('#inventarioAgregadoCartel').removeAttr('required','required');
  }

}


function validar(valor) {
  var inventario = $('#inventarioConsulta').val();
  var titulo = $('#tituloConsulta').val();
  var fechaInicio = $('#fechaInicio').val();
  var fechaFinal = $('#fechaFinal').val();
  var director = $('#directorConsulta').val();
  var estelar = $('#estelarConsulta').val();

  if(valor == 1){

  }else if(valor == 2){

  }else if(valor == 3){

  }else if(valor == 4){

  }else if(valor == 5){

  }
}

function consulta(valor) {
    if(valor == 1){
      $('#porInventario').show(600);
      $('#porTitulo').slideUp(600);
      $('#porFechas').slideUp(600);
      $('#porDirector').slideUp(600);
      $('#porEstelares').slideUp(600);
      $('#porCoincidencia').slideUp(600);
      $('#porProduccion').slideUp(600);

      $('#inventarioConsulta').attr('required','required');
      $('#tituloConsulta').removeAttr('required','required');
      $('#fechaInicio').removeAttr('required','required');
      $('#fechaFinal').removeAttr('required','required');
      $('#directorConsulta').removeAttr('required','required');
      $('#estelarConsulta').removeAttr('required','required');
      $('#nombreCoincidencia').removeAttr('required','required');
      $('#nombreProduccion').removeAttr('required','required');

    }else if(valor == 2){
      $('#porInventario').slideUp(600);
      $('#porTitulo').show(600);
      $('#porFechas').slideUp(600);
      $('#porDirector').slideUp(600);
      $('#porEstelares').slideUp(600);
      $('#porCoincidencia').slideUp(600);
      $('#porProduccion').slideUp(600);

      $('#inventarioConsulta').removeAttr('required','required');
      $('#tituloConsulta').attr('required','required');
      $('#fechaInicio').removeAttr('required','required');
      $('#fechaFinal').removeAttr('required','required');
      $('#directorConsulta').removeAttr('required','required');
      $('#estelarConsulta').removeAttr('required','required');
      $('#nombreCoincidencia').removeAttr('required','required');
      $('#nombreProduccion').removeAttr('required','required');

    }else if(valor == 3){
      $('#porInventario').slideUp(600);
      $('#porTitulo').slideUp(600);
      $('#porFechas').show(600);
      $('#porDirector').slideUp(600);
      $('#porEstelares').slideUp(600);
      $('#porCoincidencia').slideUp(600);
      $('#porProduccion').slideUp(600);

      $('#inventarioConsulta').removeAttr('required','required');
      $('#tituloConsulta').removeAttr('required','required');
      $('#fechaInicio').attr('required','required');
      $('#fechaFinal').attr('required','required');
      $('#directorConsulta').removeAttr('required','required');
      $('#estelarConsulta').removeAttr('required','required');
      $('#nombreCoincidencia').removeAttr('required','required');
      $('#nombreProduccion').removeAttr('required','required');

    }else if(valor == 4){
      $('#porInventario').slideUp(600);
      $('#porTitulo').slideUp(600);
      $('#porFechas').slideUp(600);
      $('#porDirector').show(600);
      $('#porEstelares').slideUp(600);
      $('#porCoincidencia').slideUp(600);
      $('#porProduccion').slideUp(600);

      $('#inventarioConsulta').removeAttr('required','required');
      $('#tituloConsulta').removeAttr('required','required');
      $('#fechaInicio').removeAttr('required','required');
      $('#fechaFinal').removeAttr('required','required');
      $('#directorConsulta').attr('required','required');
      $('#estelarConsulta').removeAttr('required','required');
      $('#nombreCoincidencia').removeAttr('required','required');
      $('#nombreProduccion').removeAttr('required','required');

    }else if(valor == 5){
      $('#porInventario').slideUp(600);
      $('#porTitulo').slideUp(600);
      $('#porFechas').slideUp(600);
      $('#porDirector').slideUp(600);
      $('#porEstelares').show(600);
      $('#porCoincidencia').slideUp(600);
      $('#porProduccion').slideUp(600);

      $('#inventarioConsulta').removeAttr('required','required');
      $('#tituloConsulta').removeAttr('required','required');
      $('#fechaInicio').removeAttr('required','required');
      $('#fechaFinal').removeAttr('required','required');
      $('#directorConsulta').removeAttr('required','required');
      $('#estelarConsulta').attr('required','required');
      $('#nombreCoincidencia').removeAttr('required','required');
      $('#nombreProduccion').removeAttr('required','required');

    }else if(valor == 6){
      $('#porInventario').slideUp(600);
      $('#porTitulo').slideUp(600);
      $('#porFechas').slideUp(600);
      $('#porDirector').slideUp(600);
      $('#porEstelares').slideUp(600);
      $('#porCoincidencia').slideUp(600);
      $('#porProduccion').show(600);

      $('#inventarioConsulta').removeAttr('required','required');
      $('#tituloConsulta').removeAttr('required','required');
      $('#fechaInicio').removeAttr('required','required');
      $('#fechaFinal').removeAttr('required','required');
      $('#directorConsulta').removeAttr('required','required');
      $('#estelarConsulta').removeAttr('required','required');
      $('#nombreCoincidencia').removeAttr('required','required');
      $('#nombreProduccion').attr('required','required');

    }else if(valor == 7){
      $('#porInventario').slideUp(600);
      $('#porTitulo').slideUp(600);
      $('#porFechas').slideUp(600);
      $('#porDirector').slideUp(600);
      $('#porEstelares').slideUp(600);
      $('#porCoincidencia').show(600);
      $('#porProduccion').slideUp(600);

      $('#inventarioConsulta').removeAttr('required','required');
      $('#tituloConsulta').removeAttr('required','required');
      $('#fechaInicio').removeAttr('required','required');
      $('#fechaFinal').removeAttr('required','required');
      $('#directorConsulta').removeAttr('required','required');
      $('#estelarConsulta').removeAttr('required','required');
      $('#nombreCoincidencia').attr('required','required');
      $('#nombreProduccion').removeAttr('required','required');
    }
}
