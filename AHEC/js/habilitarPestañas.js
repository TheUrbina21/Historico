
//variables para guardar array y length de array
var arDir = 0;
var arProd = 0;
var arEst = 0;
var arGen = 0;
var arPais = 0;
var arEdoC = 0;
var arIntS = 0;
var arIntR = 0;

//banderas
var flag1 = 0;
var flag2 = 0;
var flag3 = 0;
$(document).ready(function (){


  $('#registro').prop('disabled', true);
  //traer el array de maggic suggest del multiselect 'director'




  //asignar array con valores
  $(director).on('selectionchange', function(e, cb, s){
     director = cb.getValue();
  });


  //asignar array con valores
  $(produccion).on('selectionchange', function(e, cb, s){
     produccion = cb.getValue();
  });


  //asignar array con valores
  $(estelares).on('selectionchange', function(e, cb, s){
     estelares = cb.getValue();
  });



  //asignar array con valores
  $(genero).on('selectionchange', function(e, cb, s){
     genero = cb.getValue();
  });


  //asignar array con valores
  $(pais).on('selectionchange', function(e, cb, s){
     pais = cb.getValue();
  });


  //asignar array con valores
  $(conservacion).on('selectionchange', function(e, cb, s){
     edoConservacion = cb.getValue();
  });



  //asignar array con valores
  $(IntervencionS).on('selectionchange', function(e, cb, s){
     intSugerida = cb.getValue();
  });


  //asignar array con valores
  $(intervencionR).on('selectionchange', function(e, cb, s){
     intRealizada = cb.getValue();
  });
  pestaña4();
});

pestaña1();
pestaña2();
pestaña3();

//funciones
function pestaña1(){
  $("#pestaña2").click(function (){
    var numInventario = parseInt(document.getElementById('NumInventario').value);
    var titulo = document.getElementById('titulo').value;
    var volumenes = parseInt(document.getElementById('volumenes').value);
    var imagen = document.getElementById('files').value;


      if (numInventario > 0 && titulo.length > 6 && volumenes > 0 && imagen.length > 10){
        flag1 = 1;
      }else{
        flag1 = 0;
        $("#pestaña2").die();
      }
  });
}

function pestaña2(){
  $("#pestaña3").click(function(){
    var estreno = fecha1();
    arDir = director.length;
    arProd = produccion.length;
    arEst = estelares.length;
    arGen = genero.length;
    arPais = pais.length;
    if(estreno !=1){
      alert("El estreno del evento debe tener al menos 20 años de antiguedad");
    }

    if(arDir > 0 && arProd > 0 && arEst > 0 && arGen > 0 && arPais > 0 && estreno == 1){
      flag2 = 1;
      $("#pestaña3").hover(function(){
        $("#pestaña3").live();
      });
    }else{
      flag2 = 0;
      $("#pestaña3").die();
    }
  });
}

function pestaña3(){
  $("#pestaña4").click(function(){
    var alto = parseInt(document.getElementById('alto').value);
    var ancho = parseInt(document.getElementById('ancho').value);
    var cm = document.getElementById('cm').checked;
    var pul = document.getElementById('pul').checked;
    arEdoC = edoConservacion.length;
    arIntS = intSugerida.length;
    arIntR = intRealizada.length;
    alert(arEdoc);
    ardoC = conservacion.length;
    alert(arEdoc);
    if(alto > 0 && ancho > 0 && arEdoC > 0 && arIntS > 0 && arIntR > 0 && (cm == true || pul == true)){
      flag3 = 1;
      $("#pestaña4").hover(function(){
        $("#pestaña4").live();
      });
    }else{
      flag3 = 0;
      $("#pestaña4").die();
    }
  });
}

function pestaña4(){
  $('#fechaCatalogacion').change(function(){
    var consulta1 = document.getElementById('consulta1').checked;
    var consulta2 = document.getElementById('consulta2').checked;
    var reproduccion1 = document.getElementById('reproduccion1').checked;
    var reproduccion2 = document.getElementById('reproduccion2').checked;
    var digital1 = document.getElementById('digital1').checked;
    var digital2 = document.getElementById('digital2').checked;
    var fecCat = fecha2();
    var motivoConsulta = document.getElementById('motivoConsulta').value;

    if((consulta1 == true || consulta2 == true) && (reproduccion1 == true || reproduccion2 == true) && (digital1 == true || digital2 == true)
    && fecCat == 1 && flag1 == 1 && flag2 == 1 && flag3 == 1){
      if(consulta2 == true && motivoConsulta.length > 0){
        $("#aceptarMotivo").click(function (){
          if(motivoConsulta > 0){
            $('#registro').prop('disabled', false);
          }else{
            $('#registro').prop('disabled', true);
          }
        });
        $('#registro').prop('disabled', false);

        $('#pestaña1').click(function(){
          $('#registro').prop('disabled', true);
        });
        $('#pestaña2').click(function(){
          $('#registro').prop('disabled', true);
        });
        $('#pestaña3').click(function(){
          $('#registro').prop('disabled', true);
        });
      }else{
        if(consulta2 == false){
          $('#registro').prop('disabled', false);

          $('#pestaña1').click(function(){
            $('#registro').prop('disabled', true);
          });
          $('#pestaña2').click(function(){
            $('#registro').prop('disabled', true);
          });
          $('#pestaña3').click(function(){
            $('#registro').prop('disabled', true);
          });
        }else{
          $('#registro').prop('disabled', true);
        }
      }
    }else{
      $('#registro').prop('disabled', true);
    }

  });
}

function fecha1(){
  var fecInput = new Date(document.getElementById("estreno").value);
  var añoInput = parseInt(fecInput.getFullYear());

  var fecActual = new Date();
  var añoActual = parseInt(fecActual.getFullYear());
  var calculo =  añoActual-añoInput;

  if(calculo >= 20){
    return 1;
  }else{
    return 0;
  }
}

function fecha2(){
  var fecInput = new Date(document.getElementById("fechaCatalogacion").value);
  var añoInput = parseInt(fecInput.getFullYear());
  var mesInput = parseInt(fecInput.getMonth());
  var diaInput = parseInt(fecInput.getDate());

  var fecActual = new Date();
  var añoActual = parseInt(fecActual.getFullYear());
  var mesActual = parseInt(fecActual.getMonth());
  var diaActual = parseInt(fecActual.getDate());
  diaActual = diaActual - 1;

  if(añoInput < añoActual){
    return 1;
  }else{
    if(añoInput == añoActual && mesInput < mesActual){
      return 1;
    }else{
      if(añoInput == añoActual && mesInput == mesActual && diaInput < diaActual){
        return 1;
      }else{
        if(añoInput == añoActual && mesInput == mesActual && diaInput == diaActual){
          return 1;
        }else{
          return 0;
        }
      }
    }
  }
}
