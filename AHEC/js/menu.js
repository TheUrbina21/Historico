//Animación del menú
$(document).ready(function(){
  $('ul.tabs li a:first').addClass('active');
  $('.secciones article').hide();
  $('.secciones article:first').show();

  $('ul.tabs li a').click(function(){
    $('ul.tabs li a').removeClass('active');
    $(this).addClass('active');
    $('.secciones article').hide();

    var activeTab = $(this).attr('href');
    $(activeTab).show();
    return false;
  });

  $('#abrirMenu').click(function () {
    $('#sidebar').show(600);
    $('#sidebar').addClass('slideInLeft');
    $('#abrirMenu').css({
        'visibility': 'hidden'
    });
  });
  $('#cerrarMenu').click(function () {
    $('#sidebar').slideUp(600);
    $('#abrirMenu').css({
        'visibility': 'visible'
    });
  });

  $("html").click(function(){
    $('#sidebar').slideUp(600);
    $('#abrirMenu').css({
        'visibility': 'visible'
    });
  });

  $("#sidebar").click(function(e){
     e.stopPropagation();
  });

  $("#abrirMenu").click(function(e){
     e.stopPropagation();
  });

});
