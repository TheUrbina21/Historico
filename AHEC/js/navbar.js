$(document).ready(function(){
    //Se prepara el scroll de la ventana y se modifica su función
    $(window).scroll(function() {
        //en caso de que el navbar esté a más de 400px del top de la ventana se cambia la configuración automática del navbar
        if($("#navb").offset().top > 100) {
            //Se abre el archivo de estilos para darle transformación
            $('#navb').css({
                'height': '50px',
                'transition': 'all 2s ease',
                'background-color':'#AED5F1',
                'transition-timing-function': 'ease-out',
                'transition-duration': '5000ms'
            });
        } else { //en caso de ser menor 400px al top de la ventana la barra de scroll se regresa a automático todo
            //Se abre el archivo de estilos para darle transformación
            $('#navb').css({
                'height': 'auto',
                'transition': 'all 2s ease',
                'background-color':'#B3B3B3',
                'transition-timing-function': 'ease-out',
                'transition-duration': '5000ms'
            });

        }

    });
});
