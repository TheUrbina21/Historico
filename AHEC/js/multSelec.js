

//CADA VARIABLE REPRESENTA A UN ELEMNTO DROP DOWN LIST DEL HTML
//NAME REPRESENTA EL NOMBRE CON QUE SERÁ RECIBIDO EN PHP
//METHOD ES EL TIPO DE METODO POR EL QUE SERÁN ENVIADOS LOS DATOS
//EL PLACEHOLDER ES EL TEXTO DE FONDO DEL DROP DOWN LIST
//NOSUGGESTIONTEXT ES LA ETIQUETA EN LA ESQUIN SUPERIOR DERECHA DEL DROP DOWN LIST QUE APARECE CUANDO NO HAY
//SIGERENCIAS RESPECTO A LO QUE EL USUARIO HA ESCRITO
//REQUIERED INDICA SI DEBE HABER POR LO MENOS UN ELEMENTO SELECCIONADO
//DATA ES LA INSERCCION DE DATOS AL DROP DOWN LIST
//#estado_conservacion  ES EL NOMBRE DEL DIV EN EL HTML, DE ESTE MODO SE RECUPERA DEL HTML




      var motivosNoConsulta = $('#motivo_No_Consulta').magicSuggest({
        //NOMBRE DEL ELEMENTO PARA RECIBIRLO EN PHP
        name:'motivosNoConsulta',
        //METODO DE ENVIO AL SERVIDOR
        method:'post',
        //MAXIMO NUMERO DE CARACTERES EN LAS PALABRAS
        maxEntryLength: 30,
        //MAXIMO NUMERO DE ELEMENTOS SELECCIONADOS
        maxSelection: 3,
        //MENSAJE CUNADO SE ALCANZA EL NUMERO MÁXIMO DE ELEMENTOS SELECCIONADOS
        maxSelectionRenderer:  function(v) {
            return 'Ya no puedes agregar más';
          },
          //MENSAJE CUANDO SE ALCANZA EN NUMERO MÁXIMO DE CARACTERES PARA UN ELEMENTO
        maxEntryRenderer: function(v) {
            return 'No puedes agregar un nombre tan largo';
          },
          //OBTENCION DE DATOS. SE REQUIERE DE UNA FUNCION PARA PROCESAR LOS DATOS, YA QUE SE ENCUENTRAN
          //EN DOS ARRAY DISTINTOS
        data: function(){
          //VECTOR DE DATOS
          var  r = [];
          //SE REPITE TANTAS VECES COMO ELEMENTOS EXISTAN
        for(var i = 0; i <nombreMotivosNoConsulta.length; i++){
          //SE AGREGAN DOS ELEMENTOS AL VECTOR DE DATOS
          //EL PRIMER DATO ES EL ID DEL ITEM Y EL SEGUNDO ES EL NOMBRE
          r.push({id:idMotivosNoConsulta[i], name:nombreMotivosNoConsulta[i]});
        }
        //SE RETORNA EL VECTOR Y SE ALIMENTA EL ELEMENTO
        return r;
        },
        //PLACEHOLDER CUANDO EL ELEMENTO NO TIENE NINGUNA SELECCION
        placeholder: 'Seleccione o escriba los motivos',
        //MENSAJE PARA CUANDO NO HAY COINCIENCIAS DE LO QUE EL USUARIO ESCRIBIO
        noSuggestionText: '',
        //PROPIEDAD PARA QUE EL ELEMENTO SEA REQUERIDO.
        //AGREGA UN CONTORNO ROJO AL ELEMENTO SI NO SE SELECCIONA NINGUNA OPCION
        required: true
        });




        var conservacion= $('#estado_conservacion').magicSuggest({
          name:'estado_conservacion',
          method:'post',
          allowFreeEntries: false,
          maxSelection: 5,
          maxSelectionRenderer:  function(v) {
              return 'Ya no puedes agregar más';
          },
          placeholder: 'Seleccione o escriba el estado',
          noSuggestionText: '',
          maxEntryLength: 15,
          maxEntryRenderer: function(v) {
              return 'No puedes agregar un nombre tan largo';
          },
                  required: true,
          data: function(){

            //SE INICIALIZA UN VECTOR
          var  r = [];
            //INICIAMOS UN CICLO CUANTOS ELEMENTOS HAYA EN LA VARIABLE nombreDeterioros
            //LA VARIABLE VIENE DEL HTML, OBTENIDA POR LOS DATOS DE LA BASE DE DATOS
            for(var i = 0; i <nombreDeterioros.length; i++){
              //SE INSERTA EL IDDEL ELEMENTO DEL ARRAYIDDETERIOROS(OBTENIDOS DE HTML) Y EL NOMBRE CORRESPONDIENTE AL ID
              r.push({id:idDeterioros[i], name:nombreDeterioros[i]});

            }
            //UNA VEZ TERMINADO EL ARAY, SE RETORNA Y LOS DATOS SON INSERTADOS
            return r;
          }
        });


        //EL MISMO PROCESO SE REPITE PARA CADA DROP DOWN LIST

        var genero = $('#genero').magicSuggest({
          name: 'genero',
          maxSelection: 2,
          maxSelectionRenderer:  function(v) {
              return 'Ya no puedes agregar más';
          },
          method: 'post',
          maxEntryLength: 15,
          placeholder: 'Seleccione o escriba el genero',
          noSuggestionText: '',
          maxEntryRenderer: function(v) {
              return 'No puedes agregar un nombre tan largo';
          },
                  required: true,
          data: function(){
          var  r = [];

            for(var i = 0; i <nombreGeneros.length; i++){
              r.push({id:idGeneros[i], name:nombreGeneros[i]});
            }
            return r;
          }
        });

        //genero.setValue(generos);





        var estelares = $('#estelares').magicSuggest({
          name:'estelares',
          method:'post',
          //minchars:4,
          maxSelection: 3,
          maxSelectionRenderer:  function(v) {
              return 'Ya no puedes agregar más';
          },
          maxEntryLength: 60,
          minCharsRenderer:function(v){
            return 'Debes escribir por lo menos 4 letras';
          },
          maxEntryRenderer: function(v) {
              return 'No puedes agregar un nombre tan largo';
          },
          placeholder: 'Escriba o selecciones a los estelares',
          noSuggestionText: '',
                  required: true,
          data: function(){
          var  r = [];

            for(var i = 0; i <nombreEstelares.length; i++){
              r.push({id:idEstelares[i], name:nombreEstelares[i]});
            }
            return r;
          }
        });

        var pais = $('#pais').magicSuggest({
          name:'paises',
          method:'post',
          maxEntryLength: 40,
          maxSelection: 3,
          maxSelectionRenderer:  function(v) {
              return 'Ya no puedes agregar más';
          },
          maxEntryRenderer: function(v) {
              return 'No puedes agregar un nombre tan largo';
          },
          data: function(){
          var  r = [];

            for(var i = 0; i <nombrePaises.length; i++){
              r.push({id:idPaises[i], name:nombrePaises[i]});
            }
            return r;
          }
          ,
          placeholder: 'Escriba o escoja el país',
          noSuggestionText: '',
                  required: true
        });





        var intervencionR = $('#intervencionR').magicSuggest({
          name:'intervencionR',
          maxSelection: 3,
          allowFreeEntries: false,
          maxSelectionRenderer:  function(v) {
              return 'Ya no puedes agregar más';
          },
          maxEntryLength: 50,
          method:'post',
          maxEntryRenderer: function(v) {
              return 'No puedes agregar un nombre tan largo';
          },
          data: function(){
          var  r = [];

            for(var i = 0; i <nombreIntervenciones.length; i++){
              r.push({id:idIntervenciones[i], name:nombreIntervenciones[i]});
            }
            return r;
          },
          placeholder: 'Seleccione o escriba las intervenciones',
          noSuggestionText: '',
                  required: true
        });


        var intervencionS = $('#intervencionS').magicSuggest({
          name:'intervencionS',
          allowFreeEntries: false,
          maxSelection: 3,
          maxSelectionRenderer:  function(v) {
              return 'Ya no puedes agregar más';
          },
          maxEntryLength: 50,
          method:'post',
          maxEntryRenderer: function(v) {
              return 'No puedes agregar un nombre tan largo';
          },
        data: function(){
        var  r = [];

          for(var i = 0; i <nombreIntervenciones.length; i++){
            r.push({id:idIntervenciones[i], name:nombreIntervenciones[i]});
          }
          return r;
        },
        placeholder: 'Seleccione o escriba las intervenciones',
        noSuggestionText: '',
                required: true
        });






        var director = $('#director').magicSuggest({
          name:'directores',
          method:'post',
          maxEntryLength: 60,
          maxSelection: 3,
          maxSelectionRenderer:  function(v) {
              return 'Ya no puedes agregar más';
          },
          //minchars:4,
          minCharsRenderer:function(v){
            return 'Debes escribir por lo menos 4 letras';
          },
          maxEntryRenderer: function(v) {
              return 'No puedes agregar un nombre tan largo';
          },
        data: function(){
        var  r = [];

          for(var i = 0; i <nombreDirectores.length; i++){
            r.push({id:idDirectores[i], name:nombreDirectores[i]});
          }
          return r;
        },
        placeholder: 'Seleccione el director',
        noSuggestionText: '',
                required: true
        });






        var produccion = $('#produccion').magicSuggest({
          name:'produccion',
          method:'post',
          maxEntryLength: 60,
          //minchars:4,
          maxSelection: 5,
          maxSelectionRenderer:  function(v) {
              return 'Ya no puedes agregar más';
          },
          minCharsRenderer:function(v){
            return 'Debes escribir por lo menos 4 letras';
          },
          maxEntryRenderer: function(v) {
              return 'No puedes agregar un nombre tan largo';
          },
        data: function(){
        var  r = [];

          for(var i = 0; i <nombreProductores.length; i++){
            r.push({id:idProductores[i], name:nombreProductores[i]});
          }
          return r;
        },
        placeholder: 'Seleccione o escriba a los productores',
        noSuggestionText: '',
        required: true
        });
