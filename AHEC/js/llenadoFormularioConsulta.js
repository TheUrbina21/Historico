
$(document).ready(function(){

//RELLENADO DE LOS ELEMENTO CON LOS DATOS DE LA BASE DE DATOS
  $('#tituloCartel').val(datosGenerales[1]);
  $('#NumInventario').val(datosGenerales[15]);
  $('#volumenes').val(datosGenerales[19]);
  $('#estreno').val(datosGenerales[2]);
  $('#alto').val(datosGenerales[16]);
  $('#ancho').val(datosGenerales[17]);

  if(datosGenerales[18] == 0){
    $('#medidaCm').prop("checked",true);
  }else{
    $('#medidaPul').prop("checked",true);
  }

  if(datosGenerales[4] == 0){
    $('#consultaF').prop("checked",true);
  }else{
    $('#consultaV').prop("checked",true);
  }

if(datosGenerales[3]==0){
  $('#reproduccionF').prop("checked",true);
}else{
  $('#reproduccionV').prop("checked",true);
}


if(datosGenerales[7] == 0){
  $('#reproduccionDigitalF').prop("checked",true);
}else{
  $('#reproduccionDigitalV').prop("checked",true);
}

$('#observaciones').val(datosGenerales[20]);
$('#fechaCatalogacion').val(datosGenerales[5]);
$('#fechaEstreno').val(datosGenerales[2]);
$('#tipologia').val(datosGenerales[21]);
$('#fondo').val(datosGenerales[9]);
$('#coleccion').val(datosGenerales[22]);
$('#caja').val(datosGenerales[11]);
$('#estante').val(datosGenerales[10]);
$('#soporte').val(datosGenerales[12]);
$('#impresion').val(datosGenerales[23]);
$('#catalogador').val(datosGenerales[14]);
//$('#capturista').val(datosGenerales[]);
$('#archivo').val(datosGenerales[8]);


//DESABILITAR LOS ELEMENTOS
$('#tituloCartel').attr('disabled','disabled');
$('#NumInventario').attr('disabled','disabled');
$('#volumenes').attr('disabled','disabled');
$('#estreno').attr('disabled','disabled');
$('#alto').attr('disabled','disabled');
$('#ancho').attr('disabled','disabled');
$('#medidaCm').attr('disabled','disabled');
$('#medidaPul').attr('disabled','disabled');
$('#tituloCartel').attr('disabled','disabled');
$('#consultaV').attr('disabled','disabled');
$('#consultaF').attr('disabled','disabled');
$('#reproduccionV').attr('disabled','disabled');
$('#reproduccionF').attr('disabled','disabled');
$('#reproduccionDigitalV').attr('disabled','disabled');
$('#reproduccionDigitalF').attr('disabled','disabled');
$('#observaciones').attr('disabled','disabled');
$('#fechaCatalogacion').attr('disabled','disabled');
$('#fechaEstreno').attr('disabled','disabled');
$('#tipologia').attr('disabled','disabled');
$('#fondo').attr('disabled','disabled');
$('#coleccion').attr('disabled','disabled');
$('#caja').attr('disabled','disabled');
$('#estante').attr('disabled','disabled');
$('#soporte').attr('disabled','disabled');
$('#coleccion').attr('disabled','disabled');
$('#tipologia').attr('disabled','disabled');
$('#catalogador').attr('disabled','disabled');
$('#archivos').attr('disabled','disabled');

//LLENADO DE LOS DETALLES CON EL MAGICSUGGEST
  genero.setValue(INgeneros);
  estelares.setValue(INestelares);
  pais.setValue(INpaises);
  director.setValue(INdirectores);
  produccion.setValue(INproducciones);
  //DESABILITAR LOS ELEMENTOS
  genero.disable();
  estelares.disable();
  pais.disable();
  director.disable();
  produccion.disable();

});
