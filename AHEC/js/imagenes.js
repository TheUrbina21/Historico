var j = 0;
function archivo(evt) {
      var files = evt.target.files; // FileList object

        //Obtenemos la imagen del campo "file".
      for (var i = 0, f; f = files[i]; i++) {
           //Solo admitimos imágenes.
           if (!f.type.match('image.*')) {
                continue;
           }

           var reader = new FileReader();

           reader.onload = (function(theFile) {
               return function(e) {
               // Creamos la imagen.
                 //Creamos un div para poder sustitur ahí mismo la imagen, y que se vaya recorriendo
                 var divFirst = document.getElementById('first');
                 divFirst.insertAdjacentHTML("afterend","<div class='contImg' id='contImg"+ j +"'></div>");
                 //se sustituye el div anterior para mostrar la imagen en miniatura
                 var divNew = document.getElementById('contImg'+ j);
                 divNew.innerHTML = ['<div id="divDel'+j+'" class=""><img id="img'+j+'" class="thumb divImg" src="', e.target.result,'" title="', escape(theFile.name), '" data-toggle="modal" data-target="#modalImage' + j +'"><span id="close' + j +'" class=""><i class="fas fa-times acom"></i></span></img></div>'].join('');
                 //Se crea un modal para ponerlo al final del formulario y poder mostrarlo con el mismo modal
                 var divModal = document.getElementById('form');
                 divModal.insertAdjacentHTML("afterend", '<div class="modal fade" id="modalImage'+j+'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"><div class="modal-dialog modal-lg" role="document"><div class="modal-content"><div class="modal-header"><h5 class="modal-title" id="exampleModalLabel">Imagen seleccionada</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><output id="list'+j+'" class="d-flex justify-content-center"></output><div class="modal-footer"><button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button></div></div></div></div>');
                 document.getElementById("list"+j).innerHTML = ['<img class="thumb" src="', e.target.result,'" title="', escape(theFile.name), '"/>'].join('');
                 var target = e.target.result;
                 var name = escape(theFile.name);
                 cerrarImagenes(j,target,name);
                 j++;
               };

           })(f);
           reader.readAsDataURL(f);
       }
}
      document.getElementById('files').addEventListener('change', archivo, false);

      function cerrarImagenes(num, target, name) {
        $("#close"+num).on("click",function(){
          $("#divDel"+num).remove();
          $("#modalImage"+num).remove();
          $("#contImg"+num).remove();
          $("list"+num).remove();
          //alert(target);
          //alert(name);
        });
      }
