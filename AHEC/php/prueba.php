<?php
// Cargamos la foto original
$im = imagecreatefromjpeg('../images/001.jpg');

// Primero crearemos nuestra imagen de la estampa manualmente
$marca_agua = imagecreatefrompng("../images/back.png");

// Establecer los márgenes para la estampa y obtener el alto/ancho de la imagen de la estampa
$margen_dcho = 10;
$margen_inf = 10;
$sx = imagesx($marca_agua);
$sy = imagesy($marca_agua);

// Fusionar la estampa con nuestra foto con una opacidad del 50%
imagecopymerge($im, $marca_agua, 0, imagesy($im) - $sy, 0, 0, $sx, $sy, 80);

// Guardar la imagen en un archivo y liberar memoria
imagepng($im, 'nueva_imagen.png');
imagedestroy($im);


?>
