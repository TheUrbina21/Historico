<?php
//CONECCION A LA BASE DE DATOS AMAZON
$mysqli = mysqli_connect("ahec.c8febllwvki7.us-east-2.rds.amazonaws.com","miguelUrbina","rootahec","AHEC");
//$mysqli = mysqli_connect("35.188.139.254","miguelUrbina","rootahec","AHEC");
if ($mysqli->connect_errno) {
    echo "Fallo al conectar a MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
}

//CAMBIAR EL CHARSET A UTF8
if (!$mysqli->set_charset("utf8")) {
    printf("Error cargando el conjunto de caracteres utf8: %s\n", $mysqli->error);
    exit();
}



//VARIABLE FIJA DEL TIPO DE ELEMENTO QUE SE VA A REFISTRAR.
//ESTA SERÁ DE ACUERDO A LA OPCION QUE HAYA REALIZADO EL USUARIO, SIN EMBARGO POR AHORA SERÁ FIJA
$tipo=1;

//DECLARACION DE LOS ARRAY PARA LA OBTENCION DE LOS ELEMENTOS DE LA BASE DE DATOS
$idArchivos=[];//ARRAY PARA LOS ID
$nombreArchivos= [];//ARRAY PARA LOS NOMBRES
//SE HACE EL LLAMADO AL STORED PROCEDURE
if (!$mysqli->multi_query("CALL OBTENERARCHIVO()")) {
  //EN CASO DE ERROR LA CONSULTA RETORNA FALSE Y SE HACE LA PETICION DEL ERROR
    echo "Error llamando al procedure OBTENERARCHIVO: (" . $mysqli->errno . ") " . $mysqli->error;
}
//CICLO PARA LA OBTENCION DE LOS RESULTADOS
do {
    if ($res = $mysqli->store_result()) {
      //SI EL STORED PROCEDURE RETORNA UN VALOR
        $result=$res->fetch_all();//SE GUARDA EN LA VARIABLE RESULT
        $res->free();//SE LIVERA LA CONSULTA
    } else {//SI NO HAY RESULTADOS

        if ($mysqli->errno) {
            echo "Store failed: (" . $mysqli->errno . ") " . $mysqli->error;
        }
    }
} while ($mysqli->more_results() && $mysqli->next_result());

$i=0;
foreach($result as $item){
  $idArchivos[$i]=$item[0];
  $nombreArchivos[$i]=$item[1];
  $i++;
}

$idCapturistas=[];
$nombreCapturistas=[];
if (!$mysqli->multi_query("CALL OBTENERCAPTURISTA()")) {
    echo "Error llamando al procedure OBTENERCAPTURISTA: (" . $mysqli->errno . ") " . $mysqli->error;
}

do {
    if ($res = $mysqli->store_result()) {
        $result=$res->fetch_all();
        $res->free();
    } else {
        if ($mysqli->errno) {
            echo "Store failed: (" . $mysqli->errno . ") " . $mysqli->error;
        }
    }
} while ($mysqli->more_results() && $mysqli->next_result());

$i=0;
foreach($result as $item){
  $idCapturistas[$i]=$item[0];
  $nombreCapturistas[$i]=$item[1];
  $i++;
}

$idCatalogadores=[];
$nombreCatalogadores=[];
if (!$mysqli->multi_query("CALL OBTENERCATALOGADOR()")) {
    echo "Error llamando al procedure OBTENERCATALOGADOR: (" . $mysqli->errno . ") " . $mysqli->error;
}

do {
    if ($res = $mysqli->store_result()) {
        $result=$res->fetch_all();
        $res->free();
    } else {
        if ($mysqli->errno) {
            echo "Store failed: (" . $mysqli->errno . ") " . $mysqli->error;
        }
    }
} while ($mysqli->more_results() && $mysqli->next_result());

$i=0;
foreach($result as $item){
  $idCatalogadores[$i]=$item[0];
  $nombreCatalogadores[$i]=$item[1];
  $i++;
}



$idColeccion=[];
$nombreColeccion=[];
if (!$mysqli->multi_query("CALL OBTENERCOLECCION(".$tipo.")")) {
    echo "Error llamando al procedure OBTENERCOLECCION: (" . $mysqli->errno . ") " . $mysqli->error;
}

do {
    if ($res = $mysqli->store_result()) {
        $result=$res->fetch_all();
        $res->free();
    } else {
        if ($mysqli->errno) {
            echo "Store failed: (" . $mysqli->errno . ") " . $mysqli->error;
        }
    }
} while ($mysqli->more_results() && $mysqli->next_result());

$i=0;
foreach($result as $item){
  $idColeccion[$i]=$item[0];
  $nombreColeccion[$i]=$item[1];
  $i++;
}


$idDirector=[];
$nombreDirector=[];
if (!$mysqli->multi_query("CALL OBTENERDIRECTOR()")) {
    echo "Error llamando al procedure OBTENERDIRECTOR: (" . $mysqli->errno . ") " . $mysqli->error;
}

do {
    if ($res = $mysqli->store_result()) {
        $result=$res->fetch_all();
        $res->free();
    } else {
        if ($mysqli->errno) {
            echo "Store failed: (" . $mysqli->errno . ") " . $mysqli->error;
        }
    }
} while ($mysqli->more_results() && $mysqli->next_result());

$i=0;
foreach($result as $item){
  $idDirector[$i]=$item[0];
  $nombreDirector[$i]=$item[1];
  $i++;
}

$idDeterioros=[];
$nombreDeterioros=[];
if (!$mysqli->multi_query("CALL OBTENERESTADODETERIORO(".$tipo.")")) {
    echo "Error llamando al procedure OBTENERESTADODETERIORO: (" . $mysqli->errno . ") " . $mysqli->error;
}

do {
    if ($res = $mysqli->store_result()) {
        $result=$res->fetch_all();
        $res->free();
    } else {
        if ($mysqli->errno) {
            echo "Store failed: (" . $mysqli->errno . ") " . $mysqli->error;
        }
    }
} while ($mysqli->more_results() && $mysqli->next_result());

$i=0;
foreach($result as $item){
  $idDeterioros[$i]=$item[0];
  $nombreDeterioros[$i]=$item[1];
  $i++;
}


$idEstelares=[];
$nombreEstelares=[];
if (!$mysqli->multi_query("CALL OBTENERESTELAR()")) {
    echo "Error llamando al procedure OBTENERESTELAR: (" . $mysqli->errno . ") " . $mysqli->error;
}

do {
    if ($res = $mysqli->store_result()) {
        $result=$res->fetch_all();
        $res->free();
    } else {
        if ($mysqli->errno) {
            echo "Store failed: (" . $mysqli->errno . ") " . $mysqli->error;
        }
    }
} while ($mysqli->more_results() && $mysqli->next_result());

$i=0;
foreach($result as $item){
  $idEstelares[$i]=$item[0];
  $nombreEstelares[$i]=$item[1];
  $i++;
}

$idFondo=[];
$nombreFondo=[];
if (!$mysqli->multi_query("CALL OBTENERFONDO()")) {
    echo "Error llamando al procedure OBTENERFONDO: (" . $mysqli->errno . ") " . $mysqli->error;
}

do {
    if ($res = $mysqli->store_result()) {
        $result=$res->fetch_all();
        $res->free();
    } else {
        if ($mysqli->errno) {
            echo "Store failed: (" . $mysqli->errno . ") " . $mysqli->error;
        }
    }
} while ($mysqli->more_results() && $mysqli->next_result());

$i=0;
foreach($result as $item){
  $idFondo[$i]=$item[0];
  $nombreFondo[$i]=$item[1];
  $i++;
}

$idGeneros=[];
$nombreGeneros=[];
if (!$mysqli->multi_query("CALL OBTENERGENERO()")) {
    echo "Error llamando al procedure OBTENERGENERO: (" . $mysqli->errno . ") " . $mysqli->error;
}

do {
    if ($res = $mysqli->store_result()) {
        $result=$res->fetch_all();
        $res->free();
    } else {
        if ($mysqli->errno) {
            echo "Store failed: (" . $mysqli->errno . ") " . $mysqli->error;
        }
    }
} while ($mysqli->more_results() && $mysqli->next_result());

$i=0;
foreach($result as $item){
  $idGeneros[$i]=$item[0];
  $nombreGeneros[$i]=$item[1];
  $i++;
}

$idPaises=[];
$nombrePaises=[];
if (!$mysqli->multi_query("CALL OBTENERPAIS()")) {
    echo "Error llamando al procedure OBTENERPAIS: (" . $mysqli->errno . ") " . $mysqli->error;
}

do {
    if ($res = $mysqli->store_result()) {
        $result=$res->fetch_all();
        $res->free();
    } else {
        if ($mysqli->errno) {
            echo "Store failed: (" . $mysqli->errno . ") " . $mysqli->error;
        }
    }
} while ($mysqli->more_results() && $mysqli->next_result());

$i=0;
foreach($result as $item){
  $idPaises[$i]=$item[0];
  $nombrePaises[$i]=$item[1];
  $i++;
}


$idProductores=[];
$nombreProductores=[];
if (!$mysqli->multi_query("CALL OBTENERPRODUCTOR()")) {
    echo "Error llamando al procedure OBTENERPRODUCTOR: (" . $mysqli->errno . ") " . $mysqli->error;
}

do {
    if ($res = $mysqli->store_result()) {
        $result=$res->fetch_all();
        $res->free();
    } else {
        if ($mysqli->errno) {
            echo "Store failed: (" . $mysqli->errno . ") " . $mysqli->error;
        }
    }
} while ($mysqli->more_results() && $mysqli->next_result());

$i=0;
foreach($result as $item){
  $idProductores[$i]=$item[0];
  $nombreProductores[$i]=$item[1];
  $i++;
}

$idSoportes=[];
$nombreSoportes=[];
if (!$mysqli->multi_query("CALL OBTENERSOPORTE()")) {
    echo "Error llamando al procedure OBTENERSOPORTE: (" . $mysqli->errno . ") " . $mysqli->error;
}

do {
    if ($res = $mysqli->store_result()) {
        $result=$res->fetch_all();
        $res->free();
    } else {
        if ($mysqli->errno) {
            echo "Store failed: (" . $mysqli->errno . ") " . $mysqli->error;
        }
    }
} while ($mysqli->more_results() && $mysqli->next_result());

$i=0;
foreach($result as $item){
  $idSoportes[$i]=$item[0];
  $nombreSoportes[$i]=$item[1];
  $i++;
}
$idTecnicas=[];
$nombreTecnicas=[];
if (!$mysqli->multi_query("CALL OBTENERTECNICAIMPRESION()")) {
    echo "Error llamando al procedure OBTENERTECNICAIMPRESION: (" . $mysqli->errno . ") " . $mysqli->error;
}

do {
    if ($res = $mysqli->store_result()) {
        $result=$res->fetch_all();
        $res->free();
    } else {
        if ($mysqli->errno) {
            echo "Store failed: (" . $mysqli->errno . ") " . $mysqli->error;
        }
    }
} while ($mysqli->more_results() && $mysqli->next_result());

$i=0;
foreach($result as $item){
  $idTecnicas[$i]=$item[0];
  $nombreTecnicas[$i]=$item[1];
  $i++;
}


$idTipologias=[];
$nombreTipologias=[];
if (!$mysqli->multi_query("CALL OBTENERTIPOLOGIA(".$tipo.")")) {
    echo "Error llamando al procedure OBTENERTIPOLOGIA: (" . $mysqli->errno . ") " . $mysqli->error;
}

do {
    if ($res = $mysqli->store_result()) {
        $result=$res->fetch_all();
        $res->free();
    } else {
        if ($mysqli->errno) {
            echo "Store failed: (" . $mysqli->errno . ") " . $mysqli->error;
        }
    }
} while ($mysqli->more_results() && $mysqli->next_result());

$i=0;
foreach($result as $item){
  $idTipologias[$i]=$item[0];
  $nombreTipologias[$i]=$item[1];
  $i++;
}


$idCajas=[];
$nombreCajas=[];
if (!$mysqli->multi_query("CALL OBTENERCAJA()")) {
    echo "Error llamando al procedure OBTENERCAJA: (" . $mysqli->errno . ") " . $mysqli->error;
}

do {
    if ($res = $mysqli->store_result()) {
        $result=$res->fetch_all();
        $res->free();
    } else {
        if ($mysqli->errno) {
            echo "Store failed: (" . $mysqli->errno . ") " . $mysqli->error;
        }
    }
} while ($mysqli->more_results() && $mysqli->next_result());

$i=0;
foreach($result as $item){
  $idCajas[$i]=$item[0];
  $nombreCajas[$i]=$item[1];
  $i++;
}


$idEstantes=[];
$nombreEstantes=[];
if (!$mysqli->multi_query("CALL OBTENERESTANTE()")) {
    echo "Error llamando al procedure OBTENERESTANTE: (" . $mysqli->errno . ") " . $mysqli->error;
}

do {
    if ($res = $mysqli->store_result()) {
        $result=$res->fetch_all();
        $res->free();
    } else {
        if ($mysqli->errno) {
            echo "Store failed: (" . $mysqli->errno . ") " . $mysqli->error;
        }
    }
} while ($mysqli->more_results() && $mysqli->next_result());

$i=0;
foreach($result as $item){
  $idEstantes[$i]=$item[0];
  $nombreEstantes[$i]=$item[1];
  $i++;
}

$idIntervenciones=[];
$nombreIntervenciones=[];
if (!$mysqli->multi_query("CALL OBTENERINTERVENCION(".$tipo.")")) {
    echo "Error llamando al procedure OBTENERINTERVENCION: (" . $mysqli->errno . ") " . $mysqli->error;
}

do {
    if ($res = $mysqli->store_result()) {
        $result=$res->fetch_all();
        $res->free();
    } else {
        if ($mysqli->errno) {
            echo "Store failed: (" . $mysqli->errno . ") " . $mysqli->error;
        }
    }
} while ($mysqli->more_results() && $mysqli->next_result());

$i=0;
foreach($result as $item){
  $idIntervenciones[$i]=$item[0];
  $nombreIntervenciones[$i]=$item[1];
  $i++;
}





$idMotivosNoConsulta=[];
$nombreMotivosNoConsulta=[];
if (!$mysqli->multi_query("CALL OBTENERMOTIVOSDENOCONSULTA()")) {
    echo "Error llamando al procedure OBTENERMOTIVOSDENOCONSULTA: (" . $mysqli->errno . ") " . $mysqli->error;
}

do {
    if ($res = $mysqli->store_result()) {
        $result=$res->fetch_all();
        $res->free();
    } else {
        if ($mysqli->errno) {
            echo "Store failed: (" . $mysqli->errno . ") " . $mysqli->error;
        }
    }
} while ($mysqli->more_results() && $mysqli->next_result());

$i=0;
foreach($result as $item){
  $idMotivosNoConsulta[$i]=$item[0];
  $nombreMotivosNoConsulta[$i]=$item[1];
  $i++;
}






/*
if (!$mysqli->multi_query("CALL CONSULTAPORTITULO(\"Example\")")) {
    echo "Error llamando al procedure MULTISELECT: (" . $mysqli->errno . ") " . $mysqli->error;
}

do {
    if ($res = $mysqli->store_result()) {
        $result=$res->fetch_all();
        print_r($result);


        $res->free();
    } else {
        if ($mysqli->errno) {
            echo "Store failed: (" . $mysqli->errno . ") " . $mysqli->error;
        }
    }
} while ($mysqli->more_results() && $mysqli->next_result());

echo('<br>');
echo('multiselect');

foreach($result as $item){
  echo('<br>');
  print_r($item);
}

*/










//SE CIERRA SESIÓN EN LA BASE DE DATOS
mysqli_close($mysqli);




?>
