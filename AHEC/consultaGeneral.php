<?php
  include 'php/LoadData.php';


    $mysqli = mysqli_connect("ahec.c8febllwvki7.us-east-2.rds.amazonaws.com","miguelUrbina","rootahec","AHEC");
    if ($mysqli->connect_errno) {
        echo "Fallo al conectar a MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
    }

    /* cambiar el conjunto de caracteres a utf8 */
    if (!$mysqli->set_charset("utf8")) {
        printf("Error cargando el conjunto de caracteres utf8: %s\n", $mysqli->error);
        exit();
    }


//DATOS PRUEBA FIJOS
  $tipoObjeto = 1;
  if(isset($_POST['inventarioConsulta'])){
    $numeroInventario = $_POST['inventarioConsulta'];
  }else{
    $numeroEvento = $_GET['inventarioConsulta'];

    if (!$mysqli->multi_query("CALL CONSULTAPOREVENTO({$numeroEvento})")) {
        echo "Error llamando al procedure : CONSULTAPORNUMERODEINVENTARIO(" . $mysqli->errno . ") " . $mysqli->error;
    }else{
      do {
          if ($res = $mysqli->store_result()) {
              $result = $res->fetch_all();
              $res->free();
          } else {
              if ($mysqli->errno) {
                  echo "Store failed: (" . $mysqli->errno . ") " . $mysqli->error;
              }
          }
      } while ($mysqli->more_results() && $mysqli->next_result());
      $numeroInventario = $result[0][0];
    }
  }
  $result= [];
  $i=0;


  if (!$mysqli->multi_query("CALL CONSULTAPORNUMERODEINVENTARIO({$numeroInventario}, {$tipoObjeto})")) {
      echo "Error llamando al procedure : CONSULTAPORNUMERODEINVENTARIO(" . $mysqli->errno . ") " . $mysqli->error;
  }else{

    do {
        if ($res = $mysqli->store_result()) {
            $result[$i]=$res->fetch_all();
            $res->free();
            $i++;
        } else {
            if ($mysqli->errno) {
                echo "Store failed: (" . $mysqli->errno . ") " . $mysqli->error;
            }
        }
    } while ($mysqli->more_results() && $mysqli->next_result());



  }

//SE OBTIENEN LOS ELEMENTOS DE LA CONSTULTA DE LA BASE DE DATOS
$datosGenerales = $result[0][0];
$INgeneros = $result[1];
$INpaises = $result[2];
$INproducciones= $result[3];
$INdirectores = $result[4];
$INestelares = $result[5];
/*
print_r($datosGenerales);
echo('<br>');
echo(count($datosGenerales));
echo('<br>');
//echo(count($datosGenerales));
echo('<br>');
//echo(count($datosGenerales[0][0]));
echo('<br>');

print_r($INgeneros);
echo('<br>');

print_r($INpaises);
echo('<br>');

print_r($INproducciones);
echo('<br>');

print_r($INdirectores);
echo('<br>');

print_r($INestelares);
echo('<br>');

*/


//CERRAR SESION EN LA BASE DE DATOS
mysqli_close($mysqli);


?>




<!DOCTYPE html>
<html lang="es" dir="ltr">
<head>
  <meta charset="utf-8">
  <link rel="shortcut icon" href="images/Dibujo.ico">
  <title>AHEC</title>
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/styleof.css">
  <link rel="stylesheet" href="css/animate.min.css">
  <script src="js/jquery.js"></script>

  <link href="https://fonts.googleapis.com/css?family=Roboto|Roboto+Slab" rel="stylesheet">
  <script src="js/back.js"></script>
  <script src="js/menu.js"></script>
</head>
  <body>

    <div id="sidebar" class="sidebar collapse animated">
      <h4 class="animated slideInLeft">Menú<span id="cerrarMenu"><i class="fas fa-chevron-left"></i></span></h4>
      <ul>

        <li><a id="consulta" class="animated slideInRight" href="#" data-toggle="modal" data-target="#modalConsulta"><i class="fas fa-search"></i> Consultar</a></li>
        <li><a id="nuevo" href="#" class="animated slideInRight"><i class="far fa-plus-square"></i> Agregar</a></li>
        <li><a id="salir" class="deloff animated slideInRight" href="#"><i class="fas fa-sign-out-alt"></i> Cerrar Sesión</a></li>
      </ul>
      <img  src="images/back.png" width="200" class="navb d-flex align-items-end animated slideInDown" alt="">
    </div>
    <div class="contenido">
      <span id="abrirMenu"><i class="fas fa-list-ul"></i></span>
    </div>




    <div id="acordeon" class="acordeon">
      <div class="acordeon_contenedor">
        <h2 class="titulo">General</h2>
        <!-- OBTENER LOS DATOS DEL PHP DE LA CONSULTA DEL CARTEL -->


        <div class="collapse cont">

        <div class="col-sm-12 row d-flex justify-content-center">


          <div class="col-sm-12 row form-group d-flex justify-content-between">
            <div class="col-sm-6 row d-flex justify-content-center align-items-center">

              <label for="numero_inventario" class="col-sm-6 form-control-label">Número de inventario:</label>
              <input id="NumInventario" type="number" class="col-sm-4 form-control" name="numero_inventario" min="0" max="99999" placeholder="#" required="required">
            </div>
            <div class="col-sm-6 row d-flex justify-content-center align-items-center">
              <label for="tituloCartel" class="col-sm-2 form-control-label">Título:</label>
              <input class="form-control col-sm-10" id="tituloCartel" placeholder="Título del filme" type="text" name="tituloCartel" required="required">
            </div>
          </div>

          <div class="form-group row col-sm-4 d-flex align-items-center">
            <label for="tipología" class="col-sm-4 form-control-label">Tipologia:</label>
              <select class="select-class form-control col-sm-8 point" id="tipologia" name="tipologia">
                <script>
                    var nombreTipologias=<?php echo json_encode($nombreTipologias);?>;
                    var idTipologias=<?php echo json_encode($idTipologias);?>;

                    for(var row = 0; row < nombreTipologias.length; row ++ ){
                      document.write("<option value=\""+idTipologias[row] +"\" >"+nombreTipologias[row]+" </option>");
                    }

                    var datosGenerales=<?php echo json_encode($datosGenerales);?>;
                    //alert(datosGenerales[1]);
                    var INgeneros=<?php echo json_encode($INgeneros);?>;
                    var INpaises=<?php echo json_encode($INpaises);?>;
                    var INproducciones=<?php echo json_encode($INproducciones);?>;
                    var INdirectores=<?php echo json_encode($INdirectores);?>;
                    var INestelares=<?php echo json_encode($INestelares);?>;

                </script>
              </select>
          </div>
          <div class="form-group row col-sm-4 d-flex align-items-center">
            <label for="fondo" class="col-sm-3 form-control-label">Fondo:</label>
              <select class="select-class form-control col-sm-9 point" id="fondo" name="fondo">
                <script>

                    var nombreFondos=<?php echo json_encode($nombreFondo);?>;
                    var idFondos=<?php echo json_encode($idFondo);?>;

                    for(var row = 0; row < nombreFondos.length; row ++ ){
                      document.write("<option value=\""+idFondos[row] +"\" >"+nombreFondos[row]+" </option>");
                    }
                </script>
              </select>

          </div>

          <div class="form-group col-sm-4 row d-flex align-items-center">
            <label for="coleccion" class="col-sm-4 form-control-label">Colección:</label>
              <select class="select-class form-control col-sm-8 point" id="coleccion" name="coleccion">
                <script>
                    var nombreColecciones=<?php echo json_encode($nombreColeccion);?>;
                    var idColecciones=<?php echo json_encode($idColeccion);?>;

                    for(var row = 0; row < nombreColecciones.length; row ++ ){
                      document.write("<option value=\""+idColecciones[row] +"\" >"+nombreColecciones[row]+" </option>");
                    }
                </script>
              </select>
          </div>
        </div>
        <br>

            <div class="form-group row col-sm-12 d-flex justify-content-center" id="contenedor_topografico">

              <div class="form-group row col-sm-3 d-flex justify-content-center d-flex align-items-center">
                <label for="caja" class="col-sm-4 form-control-label">Caja:</label>
                <select class="select-class form-control col-sm-4 point" id="caja" name="caja">
                  <script>
                      var cajas=<?php echo json_encode($idCajas);?>;
                      var numeros=<?php echo json_encode($nombreCajas);?>;
                      for(var row = 0; row < cajas.length; row ++ ){
                        document.write("<option value=\""+numeros[row] +"\" >"+cajas[row]+" </option>");
                      }
                  </script>
                </select>

              </div>

              <div class="form-group row col-sm-3 d-flex justify-content-center d-flex align-items-center">
                <label for="estante" class="col-sm-5 form-control-label">Estante:</label>
                <select class="select-class form-control col-sm-4 point" id="estante" name="estante">
                  <script>

                      var estantes=<?php echo json_encode($idEstantes);?>;
                      var numeroEstantes=<?php echo json_encode($nombreEstantes);?>;

                      for(var row = 0; row < estantes.length; row ++ ){
                        document.write("<option value=\""+numeroEstantes[row] +"\" >"+estantes[row]+" </option>");
                      }

                  </script>
                </select>
                <!--<button type="button" class="btn btn-blur" data-toggle="modal" data-target="#modalEstante">+</button>-->
              </div>

              <div class="form-group row col-sm-3 d-flex align-items-center">
                <label for="volumenes" class="col-sm-6 form-control-label">Volumenes:</label>
                <input id="volumenes" type="number" name="volumenes" value="1" class="col-sm-4 form-control ml-2">
              </div>
            </div>

            <h5 class="text-center">Seleccione la imagen correspondiente</h5>
            <div class="d-flex justify-content-center">
              <input type="file" id="files" name="files[]" class="point">
              <br>
              <button type="button" class="btn btn-blur ml-2" data-toggle="modal" data-target="#modalImage" name="btnModal" id="btnModal">Ver</button>
            </div>
        </div>
      </div>
      <div class="acordeon_contenedor">
        <h2 class="titulo">Descripción</h2>
        <div class="collapse cont">

          <div class="col-sm-12 row d-flex justify-content-start">
            <div class="form-group col-sm-7 row d-flex justify-content-center d-flex align-items-center">
              <label class="form-control-label col-sm-4" for="fecha_donacion">Fecha de estreno:</label>
              <input id="fechaEstreno" class="col-sm-4 form-control ml-2" type="date" name="fecha_estreno" required="required">
            </div>
          </div>

          <div class="col-sm-12 row d-flex justify-content-center">
            <div class="col-sm-2 row d-flex justify-content-end align-items-center">
              <label class="form-control-label col-sm-9" for="director">Director:</label>
            </div>
            <div class="form-group row col-sm-8 ml-2">
              <div id="director" class="form-control"></div>
              <script>
                  var nombreDirectores=<?php echo json_encode($nombreDirector);?>;
                  var idDirectores=<?php echo json_encode($idDirector);?>;
              </script>
            </div>
          </div>

          <div class="col-sm-12 row d-flex justify-content-center">
            <div class="col-sm-2 row d-flex justify-content-end align-items-center">
              <label class="form-control-label col-sm-9"for="produccion">Producción:</label>
            </div>
            <div class="form-group row col-sm-8 ml-2">
              <div id="produccion" class="form-control"></div>
              <script>
                var nombreProductores=<?php echo json_encode($nombreProductores);?>;
                var idProductores=<?php echo json_encode($idProductores);?>;
              </script>
            </div>
          </div>


          <div class="col-sm-12 row d-flex justify-content-center">
            <div class="col-sm-2 row d-flex justify-content-end align-items-center">
              <label for="estelares" class="form-control-label col-sm-9">Estelares:</label>
            </div>
            <div class="form-group row col-sm-8 ml-2">
              <div id="estelares" class="form-control"></div>
              <script>
                var nombreEstelares=<?php echo json_encode($nombreEstelares);?>;
                var idEstelares=<?php echo json_encode($idEstelares);?>;
              </script>
            </div>
          </div>




          <div class="col-sm-12 row d-flex justify-content-center">
            <div class="col-sm-2 row d-flex justify-content-end align-items-center">
              <label class="form-control-label col-sm-9" for="genero">Genero:</label>
            </div>
            <div class="form-group row col-sm-8 ml-2">
              <div id="genero" class="form-control" name="genero"></div>
              <script>
                var nombreGeneros=<?php echo json_encode($nombreGeneros);?>;
                var idGeneros=<?php echo json_encode($idGeneros);?>;


              </script>
            </div>


          </div>



          <div class="col-sm-12 row d-flex justify-content-center">
            <div class="col-sm-2 row d-flex justify-content-end align-items-center">
              <label for="pais" class="form-control-label col-sm-9">País:</label>
            </div>
            <div class="form-group row col-sm-8 ml-2">
              <div class="form-control" id="pais">    </div>
              <script>
                var nombrePaises=<?php echo json_encode($nombrePaises);?>;
                var idPaises=<?php echo json_encode($idPaises);?>;
              </script>
            </div>
          </div>

        </div>
      </div>
      <div class="acordeon_contenedor">
        <h2 class="titulo">Características</h2>
        <div class="collapse cont">

          <div class="from-group row col-sm-12 d-flex justify-content-center">

            <div class="form-group row col-sm-4 d-flex justify-content-center d-flex align-items-center">
              <label for="soporte" class="form-control-label col-sm-4">Soporte:</label>
              <select class="select-class form-control col-sm-8 point" name="soporte" id="soporte" >
                <script>
                    var nombreSoportes=<?php echo json_encode($nombreSoportes);?>;
                    var idSoportes=<?php echo json_encode($idSoportes);?>;

                    for(var row = 0; row < nombreSoportes.length; row ++ ){
                      document.write("<option value=\""+idSoportes[row] +"\" >"+nombreSoportes[row]+" </option>");
                    }

                </script>
              </select>
              <!--<button type="button" class="btn btn-blur" data-toggle="modal" data-target="#modalSoporte">+</button>-->
            </div>

            <div class="form-group row col-sm-4 d-flex justify-content-center d-flex align-items-center">
              <label for="impresion" class="form-control-label col-sm-4">Técnica:</label>
              <select class="select-class form-control col-sm-8 point" name="impresion" id="impresion">
                <script>
                    var nombreTecnicas=<?php echo json_encode($nombreTecnicas);?>;
                    var idTecnicas=<?php echo json_encode($idTecnicas);?>;

                    for(var row = 0; row < nombreTecnicas.length; row ++ ){
                      document.write("<option value=\""+idTecnicas[row] +"\" >"+nombreTecnicas[row]+" </option>");
                    }

                </script>
              </select>
              <!--<button type="button" class="btn btn-blur" data-toggle="modal" data-target="#modalTecnica">+</button>-->
            </div>



          </div>

          <div class="from-group row col-sm-12 d-flex justify-content-center">
            <div class="form-group row col-sm-4 d-flex justify-content-center">
              <div class="form-group row col-sm-12 d-flex align-items-center">
                <label for="alto" class="form-control-label col-sm-3">Alto:</label>
                <input id="alto" type="number" name="alto" value="0.0" step="0.1" min="0" class="form-control col-sm-4" ind="alto" required="required">
              </div>
            </div>

            <div class="form-group row col-sm-4 d-flex justify-content-center">
              <div class="form-group row col-sm-12 d-flex align-items-center">
                <label for="ancho" class="form-control-label col-sm-4">Ancho:</label>
                <input type="number" name="ancho" value="0.0" step="0.1" min="0" class="form-control col-sm-4" id="ancho" required="required">
              </div>
            </div>

            <div class="form-group col-sm-4 row d-flex justify-content-center align-items-center">
              <label for="consulta" class="col-sm-6">Tipo medidas:  </label>
              <label class="radio-inline col-sm-2 point"><input id="medidaCm" type="radio" value="false" name="consulta" class="point" >Cm</label>
              <label class="radio-inline col-sm-2 point"><input id="medidaPul" type="radio" value="true" name="consulta" class="point" >Pul</label>
            </div>


          </div>

          <div class="col-sm-12 row d-flex justify-content-start align-items-center ml-4">
            <div class="col-sm-3 row d-flex justify-content-center align-items-center">
              <label for="conservacion" class="form-control-label">Estado de conservación:</label>
            </div>
            <div class="form-group row col-sm-8">
              <div class="form-control" id="estado_conservacion">    </div>
              <script>
              var nombreDeterioros=<?php echo json_encode($nombreDeterioros);?>;
              var idDeterioros=<?php echo json_encode($idDeterioros);?>
              </script>
            </div>
          </div>

          <div class="col-sm-12 row d-flex justify-content-center">
            <div class="col-sm-2 row d-flex justify-content-end align-items-center">
              <label for="intervencionS" class="form-control-label">Interv. sugerida:</label>
            </div>
            <div class="form-group row col-sm-8 ml-1">
              <div class="form-control" id="intervencionS">    </div>
              <script>
               var nombreIntervenciones=<?php echo json_encode($nombreIntervenciones);?>;
               var idIntervenciones=<?php echo json_encode($idIntervenciones);?>;
              </script>
            </div>
          </div>

          <div class="col-sm-12 row d-flex justify-content-center">
            <div class="col-sm-2 row d-flex justify-content-end align-items-center">
              <label for="intervencionR" class="form-control-label">Interv. realizada:</label>
            </div>
            <div class="form-group row col-sm-8 ml-1">
              <div class="form-control" id="intervencionR">    </div>
              <script>
               var nombreIntervenciones=<?php echo json_encode($nombreIntervenciones);?>;
               var idIntervenciones=<?php echo json_encode($idIntervenciones);?>;
              </script>
            </div>
          </div>

        </div>
      </div>
      <div class="acordeon_contenedor">
        <h2 class="titulo">Accesibilidad</h2>
        <div class="collapse cont">

          <div class="form-group col-sm-12 row d-flex justify-content-center">
            <div class="form-group col-sm-4 row d-flex justify-content-center align-items-center">
              <label for="consulta" class="col-sm-6">Permite consulta:  </label>
              <label class="radio-inline col-sm-2 point"><input id="consultaV" type="radio" name="consulta" value="true" class="point"> Sí</label>
              <label class="radio-inline col-sm-2 point"><input id="consultaF" type="radio" name="consulta" value="false" class="point" data-toggle="modal" data-target="#modalMotivoConsulta"> No</label>
            </div>
            <div class="form-group col-sm-4 row d-flex justify-content-center align-items-center">
              <label for="reproduccion" class="col-sm-7">Permite reproduccion:  </label>
              <label class="radio-inline col-sm-2 point"><input id="reproduccionV" type="radio" name="reproduccion" value="true"  class="point"> Sí</label>
              <label class="radio-inline col-sm-2 point"><input id="reproduccionF" type="radio" name="reproduccion" value="false" class="point"> No</label>
            </div>
            <div class="form-group col-sm-4 row d-flex justify-content-center align-items-center">
              <label for="digital" class="col-sm-7">Reproduccion digital disponible:  </label>
              <label class="radio-inline col-sm-2 point"><input id="reproduccionDigitalV" type="radio" name="digital" value="true" class="point"> Sí</label>
              <label class="radio-inline col-sm-2 point"><input id="reproduccionDigitalF" type="radio" name="digital" value="false" class="point"> No</label>
            </div>
          </div>

          <div class=" col-sm-12 row d-flex justify-content-start align-items-center ml-4">
            <div class=" col-sm-3 row d-flex justify-content-center align-items-center">
              <label id="lblNoConsulta" for="conservacion" class="collapse form-control-label">Motivo:</label>
            </div>
            <div id="selectNoConsulta" class="collapse form-group row col-sm-8">
              <div class="form-control" id="motivo_No_Consulta"></div>
              <script>
                var nombreMotivosNoConsulta=<?php echo json_encode($nombreMotivosNoConsulta);?>;
                var idMotivosNoConsulta=<?php echo json_encode($idMotivosNoConsulta);?>;

              </script>
            </div>
          </div>



          <div class="col-sm-12 d-flex justify-content-center">
            <label for="observaciones">Observaciones:</label>
            <textarea class="form-control col-sm-8 ml-1" rows="2" placeholder="Escriba las observaciones del fondo" id="observaciones"></textarea>
          </div>

          <br>


          <div class="form-group row col-sm-12 d-flex justify-content-center">
            <div class="col-sm-6 row d-flex justify-content-center">
              <div class="col-sm-4 row d-flex justify-content-end align-items-center">
                <label for="catalogador">Catalogador:</label>
              </div>
              <select class="form-control col-sm-8 ml-1 point" id="catalogador" name="catalogador">
                <script>
                var nombreCatalogadores=<?php echo json_encode($nombreCatalogadores);?>;
                var idCatalogadores=<?php echo json_encode($idCatalogadores);?>;

                for(var row = 0; row < nombreCatalogadores.length; row ++ ){
                  document.write("<option value=\""+idCatalogadores[row] +"\" >"+nombreCatalogadores[row]+" </option>");
                }

              </script>
            </select>
          </div>

          <div class="col-sm-6 row d-flex justify-content-center">
            <div class="col-sm-4 row d-flex justify-content-end align-items-center">
              <label for="capturista" class="">Capturista:</label>
            </div>
            <select class="form-control col-sm-8 ml-1 point" id="capturista" name="capturista">
              <script>
              var nombreCapturistas=<?php echo json_encode($nombreCapturistas);?>;
              var idCapturistas=<?php echo json_encode($idCapturistas);?>;

              for(var row = 0; row < nombreCapturistas.length; row ++ ){
                document.write("<option value=\""+idCapturistas[row] +"\" >"+nombreCapturistas[row]+" </option>");
              }

            </script>
            </select>
          </div>

          </div>

          <div class="form-group row d-flex justify-content-end justify-content-center">
            <div class="form-group row col-sm-5 justify-content-center align-items-center">
              <label for="archivo" class="form-control-label col-sm-3">Archivo: </label>
              <select class="select-class form-control col-sm-4 point" name="archivo" id="archivo">
                <script>
                var nombreArchivos=<?php echo json_encode($nombreArchivos);?>;
                var idArchivos=<?php echo json_encode($idArchivos);?>;
                for(var row = 0; row < nombreArchivos.length; row ++ ){
                  document.write("<option value=\""+idArchivos[row] +"\" >"+nombreArchivos[row]+" </option>");
                }

              </script>
            </select>
          </div>
          <div class="col-sm-5 row d-flex justify-content-center align-items-center">
            <label for="fecha_catalogador" class="form-control-label col-sm-6">Fecha catalogación:</label>
            <input type="date" name="fecha_catalogador" class="form-control col-sm-6" id="fechaCatalogacion">
          </div>
          </div>

        </div>
      </div>
    </div>

    <footer>
      <div class="row">
        <div class="col-sm-3 float-right">
            <img  src="images/cultura_colima.png" width="200" class="float-right" alt="">
        </div>
        <div class="col-sm-6">
          <br>
          <div class="text-center">Juárez y Díaz Mirón s/n Colima, Col. CP 28000. Colonia Centro</div>
          <div class="text-center">Tel. (312) 313 9993</div>
          <div class="text-center">info@archivohistoricocolima.mx</div>
          <div class="text-center">© 2018 Secretaría de Cultura</div>
          <div class="text-center">Gobierno del Estado de Colima</div>
        </div>
        <div class="col-sm-3 float-left">
            <img  src="images/secretaria_de_cultura.jpg" width="200" class="float-left" alt="">
        </div>
      </div>

    </footer>


    <!--Este "script" es para los iconos-->
    <script defer src="https://use.fontawesome.com/releases/v5.0.12/js/all.js" integrity="sha384-Voup2lBiiyZYkRto2XWqbzxHXwzcm4A5RfdfG6466bu5LqjwwrjXCMBQBLMWh7qR" crossorigin="anonymous"></script>
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link href="MagicSuggest/magicsuggest-min.css" rel="stylesheet">
    <script src="MagicSuggest/magicsuggest-min.js"></script>
    <script src="js/navbar.js"></script></script>
    <script src="js/magicsuggest.js"></script>
    <script type="text/javascript" src="js/multSelec.js"></script>
    <script type="text/javascript" src="js/llenadoFormularioConsulta.js"></script>
    <script type="text/javascript" src="js/images.js"></script>
    <script type="text/javascript" src="js/acordeon.js"></script>
    <script type="text/javascript" src="js/noConsulta.js"></script>

  </body>
</html>
